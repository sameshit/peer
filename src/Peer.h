//
//  Peer.h
//  Peer
//
//  Created by Oleg on 29.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef Peer_Peer_h
#define Peer_Peer_h

#include "buffer/BufferStatistic.h"

namespace PeerLib
{
    class Peer
    :public BufferStatistic
    {
    public:
        Peer(CoreObjectLib::CoreObject *core);
        virtual ~Peer();

        void PeerDeleteFromLoop();
    private:
        bool _deleted;
    };
}

#endif
