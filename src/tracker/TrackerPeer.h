//
//  TrackerPeer.h
//  Peer
//
//  Created by Oleg on 15.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__TrackerPeer__
#define __Peer__TrackerPeer__

#include "TrackerMessageHandle.h"

namespace PeerLib
{
    class TrackerPeer
    :public TrackerMessageHandle
    {
    public:
        TrackerPeer(CoreObjectLib::CoreObject *core)
        :TrackerMessageHandle(core)
        {
        
        }
        virtual ~TrackerPeer()
        {
        
        }
    };
}

#endif /* defined(__Peer__TrackerPeer__) */
