//
//  BufferPeerProcess.h
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferPeerProcess__
#define __Peer__BufferPeerProcess__

#include "BufferDeserialize.h"

namespace PeerLib
{
    class BufferProcess
    :public BufferDeserialize
    {
    public:
        BufferProcess(CoreObjectLib::CoreObject *core);
        virtual ~BufferProcess();
    protected:
        virtual bool UpdateBuffer(const uint32_t &chunk_id, const uint8_t *chunk_data) = 0;
        virtual bool UpdateRequest(MProtoLib::MSession *session,TotalRequestInfo *total_request_info,
                                   const uint32_t &chunk_id) = 0;
        virtual void UpdatePeerStatistic() = 0;
        virtual void UpdateStreamerStatistic() = 0;
        virtual void UpdateChunkInfoCounter() = 0;
    private:
        void ProcessPeerRequest(MProtoLib::MSession *session,const ChunkInfo &request);
        void ProcessResponse(MProtoLib::MSession *session,
                                     const uint32_t& chunk_id, const uint8_t *chunk_data);
    };
}

#endif /* defined(__Peer__BufferPeerProcess__) */
