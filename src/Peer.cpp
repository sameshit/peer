//
//  Peer.cpp
//  Peer
//
//  Created by Oleg on 14.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "Peer.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

Peer::Peer(CoreObject *core)
:BufferStatistic(core),_deleted(false)
{

}

Peer::~Peer()
{
    PeerDeleteFromLoop();
}

void Peer::PeerDeleteFromLoop()
{
    if (_deleted)
        return;
    
    PeerDeleteRequestTimer();
    PeerDeleteStatisticTimer();
    MProtoDeleteFromLoop();
    _deleted = true;
}