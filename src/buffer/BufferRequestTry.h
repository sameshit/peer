//
//  BufferInfo.h
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__RequestTry__
#define __Peer__RequestTry__

#include "BufferChunkInfo.h"

/// RESET BUFFER

namespace PeerLib
{
    enum CongestionState
    {
        CongestionReset,
        CongestionIncrease,
        CongestionDecrease,
    };
    
    class BufferRequestTry
    :public BufferChunkInfo
    {
    public:
        BufferRequestTry(CoreObjectLib::CoreObject *core);
        virtual ~BufferRequestTry();
    protected:
        virtual void CongestionControl(MProtoLib::MSession *session,TotalRequestInfo *total_request_info,
                                       RequestInfo *request_info,const CongestionState &congestion_state) = 0;
        
    private:
        uint32_t _next_request_id;
        bool _is_next_request_id_set;
        
        void TryRequest   (MProtoLib::MSession *session,TotalRequestInfo *total_request_info);
        bool UpdateRequest(MProtoLib::MSession *session,TotalRequestInfo *total_request_info,const uint32_t &chunk_id);
        
        friend class BufferStatistic;
        friend class BufferHybridControl;
        friend class BufferCongestion;
        friend class BufferOutput;
    };
}

#endif /* defined(__Peer__BufferInfo__) */
