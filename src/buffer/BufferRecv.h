//
//  BufferRecv.h
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferRecv__
#define __Peer__BufferRecv__

#include "BufferRegister.h"

namespace PeerLib
{
    class BufferRecv
    :public BufferRegister
    {
    public:
        BufferRecv(CoreObjectLib::CoreObject *core);
        virtual ~BufferRecv();
    private:        
        MPROTO_MESSAGE_PROC(ProcessPeerNonReliable);
        MPROTO_MESSAGE_PROC(ProcessStreamerNonReliable);
    protected:
        MPROTO_MESSAGE_VPROC(DeserializePeerChunkInfo);
        MPROTO_MESSAGE_VPROC(DeserializePeerRequest);
        MPROTO_MESSAGE_VPROC(DeserializeStreamerChunkInfo);
        MPROTO_MESSAGE_VPROC(DeserializeResponse);
    };
    
    enum PeerMessageType
    {
        PeerMessageBufferInfo = 1,
        PeerMessageRequest,
        PeerMessageResponse,
    };
}

#endif /* defined(__Peer__BufferRecv__) */
