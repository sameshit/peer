//
//  SeveralPeersTest.h
//  Peer
//
//  Created by Oleg on 07.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__SeveralPeersTest__
#define __Peer__SeveralPeersTest__

#include "../../src/Peer.h"

namespace PeerLib
{
    class SeveralPeersTest
    :public Peer
    {
    public:
        SeveralPeersTest(CoreObjectLib::CoreObject *core);
        virtual ~SeveralPeersTest();
        
        void Run();
    private:
        Peer *_peer1,*_peer2;
        CoreObjectLib::TimeTask _reconnect_task;
        
        void ParseChunk(const uint32_t &chunk_id,const uint8_t *chunk) {}
        
        MPROTO_CONNECT_PROC(TrackerConnected);
        MPROTO_CONNECT_PROC(TrackerDisconnected);
        MPROTO_CONNECT_PROC(StreamerConnected);
        MPROTO_CONNECT_PROC(StreamerDisconnected);
        MPROTO_CONNECT_PROC(PeerConnected);
        MPROTO_CONNECT_PROC(PeerDisconnected);
        void StreamPlayed();
        void StreamClosed();
    };
}

#endif /* defined(__Peer__SeveralPeersTest__) */
