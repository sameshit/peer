//
//  SimpleTest.cpp
//  Peer
//
//  Created by Oleg on 29.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "SimpleTest.h"

using namespace PeerLib;
using namespace MProtoLib;
using namespace CoreObjectLib;

SimpleTest::SimpleTest(CoreObject *core,const uint16_t &port)
:Peer(core),_port(port)
{
    OnTrackerConnect.Attach(this,&SimpleTest::TrackerConnected);
    OnTrackerDisconnect.Attach(this, &SimpleTest::TrackerDisconnected);
    OnStreamerConnect.Attach(this, &SimpleTest::StreamerConnected);
    OnStreamerDisconnect.Attach(this, &SimpleTest::StreamerDisconnected);
    OnPeerConnect.Attach(this,&SimpleTest::PeerConnected);
    OnPeerDisconnect.Attach(this, &SimpleTest::PeerDisconnected);
    
    _reconnect_task.SetTimeout(2000);
    _reconnect_task.OnTimeTask.Attach(this, &SimpleTest::Run);
    
    OnPlayStream.Attach(this, &SimpleTest::StreamPlayed);
    OnCloseStream.Attach(this, &SimpleTest::StreamClosed);
    
//    PeerSettings()->log_unsuccesful_responses = false;
    PeerSettings()->log_streamer_chunk_info  = true;
//    PeerSettings()->log_peer_chunk_info = true;
    PeerSettings()->log_hybrid_control = true;
    PeerSettings()->log_super_peer = true;
//    PeerSettings()->log_outcoming_peer_requests   = true;
//    PeerSettings()->log_outcoming_streamer_requests = true;
//    PeerSettings()->log_streamer_responses = true;
//    PeerSettings()->log_peer_responses = true;
//    PeerSettings()->log_peer_chunk_info = true;
    PeerSettings()->log_statistic = true;
//    PeerSettings()->log_request_timeout = true;
}

SimpleTest::~SimpleTest()
{
    
}

void SimpleTest::Run()
{
    Bind(_port);
    ConnectToTracker("199.231.211.56", 12345, 12466, "199.188.72.9", 12344);
//    ConnectToTracker("198.7.57.154", 12345);
}

void SimpleTest::TrackerConnected(MSession *session)
{
    LOG_VERBOSE("Connected to tracker "<<PRINT_SESSION_ADDR(session));
    PlayStream("simpsons_lq");
    
//    session->SetConnectionTimeout(1000*60*60);
}

void SimpleTest::TrackerDisconnected(MSession *session)
{
    LOG_VERBOSE("Disconnected from tracker "<<PRINT_SESSION_ADDR(session)<<" with reason: "<<
    session->GetDisconnectReason().str()<<". Reconnecting in 2 seconds.");
    _core->GetScheduler()->Add(&_reconnect_task);
}

void SimpleTest::StreamerConnected(MSession *session)
{
    LOG_VERBOSE("Streamer "<<PRINT_SESSION_ADDR(session)<<" connected");
    
//    session->SetConnectionTimeout(1000*60*60);
}

void SimpleTest::StreamerDisconnected(MSession *session)
{
    LOG_VERBOSE("Streamer "<<PRINT_SESSION_ADDR(session)<<" disconnected with reason: "
    <<session->GetDisconnectReason().str());
}

void SimpleTest::PeerConnected(MSession *session)
{
    LOG_VERBOSE("Peer "<<PRINT_SESSION_ADDR(session)<<" connected");
    
//    session->SetConnectionTimeout(1000*60*60);
}

void SimpleTest::PeerDisconnected(MSession *session)
{
    LOG_VERBOSE( "Peer "<<PRINT_SESSION_ADDR(session)<< " disconnected with reason: "
    <<session->GetDisconnectReason().str());
}

void SimpleTest::StreamPlayed()
{
    LOG_VERBOSE("Stream started to play");
}

void SimpleTest::StreamClosed()
{
    LOG_VERBOSE("Stream closed");
}