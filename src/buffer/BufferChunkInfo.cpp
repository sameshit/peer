//
//  BufferChunkInfo.cpp
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferChunkInfo.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

BufferChunkInfo::BufferChunkInfo(CoreObject *core)
:BufferProcess(core),_chunks_counter(0)
{
    fast_new(_chunk_info_timer,_core,200);
//    _chunk_info_timer->OnTimer.Attach(this, &BufferChunkInfo::SendChunkInfoToAll);
}

BufferChunkInfo::~BufferChunkInfo()
{
    fast_delete(_chunk_info_timer);
}

void BufferChunkInfo::UpdateChunkInfoCounter()
{
    ++_chunks_counter;
    if (_chunks_counter > 10)
    {
        _chunks_counter = 0;
        SendChunkInfoToAll();
    }
}

void BufferChunkInfo::SendChunkInfoToAll()
{
    MSession *session;
    DataPair data_pair;
    PeerMessageType type;
    
    type = PeerMessageBufferInfo;
    data_pair = SerializeChunkInfo();
    if (data_pair.second == 0)
        return;
    for (auto it = SessionsBegin();
         it != SessionsEnd();
         ++it)
    {
        session = (*it).second;
        if (session != GetStreamerSession()
            && session != GetTrackerSession()
            && session->GetPackedAddr() != GetStun1Addr()
            && session->GetPackedAddr() != GetStun2Addr())
            SendNonReliable(session, (uint8_t*)&type, 1, data_pair.first, data_pair.second);
    }
}

void BufferChunkInfo::SendChunkInfo(MSession *session)
{
    DataPair data_pair;
    PeerMessageType type;
    
    type = PeerMessageBufferInfo;
    data_pair = SerializeChunkInfo();
    if (data_pair.second != 0)
        SendNonReliable(session, (uint8_t*)&type, 1, data_pair.first, data_pair.second);
}

DataPair BufferChunkInfo::SerializeChunkInfo()
{
    ChunkInfo seconds;
    ChunkInfoIterator seconds_it,streamer_seconds_it;
    BufferIterator buf_it1,buf_it2,next_buf_it,it;
    uint32_t first_id,end_id,last_id,cur_id,diff;
    ssize_t size;
    uint8_t *pos;
    
    if (_buffer.empty())
        return make_pair<uint8_t*,ssize_t>(NULL, 0);
    
    buf_it1 = _buffer.begin();
    buf_it2 = _buffer.begin();
    
    for (next_buf_it = buf_it2,++next_buf_it;
         next_buf_it != _buffer.end() && (*next_buf_it).first == (*buf_it2).first + 1;
         buf_it2 = next_buf_it, ++next_buf_it);
    
    Utils::PutBe32(_chunk_info_data     ,(*buf_it1).first);
    Utils::PutBe32(_chunk_info_data +4  ,(*buf_it2).first);
    
    first_id = (*buf_it1).first;
    last_id  = (*buf_it2).first;
    end_id   = (*_buffer.rbegin()).first;
    
    if (end_id != last_id)
        size = 8 + ((end_id - last_id - 1) / 8) + 1;
    else
        size = 8;
    
    memset(_chunk_info_data+8,0,size-8);
    for (it  = buf_it2,++it;
         it != _buffer.end();
         ++it)
    {
        cur_id = (*it).first;
        diff = cur_id - last_id - 1;
        pos = _chunk_info_data + 8 + (diff / 8);
        *pos |= 1 << (diff % 8);
    }
    
    
    return make_pair(_chunk_info_data,size);
}