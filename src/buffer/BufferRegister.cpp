//
//  BufferRegister.cpp
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferRegister.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

BufferRegister::BufferRegister(CoreObject *core)
:BufferBase(core)
{}

BufferRegister::~BufferRegister()
{
    StreamerInfo *streamer_info;
    PeerInfo *peer_info;
    
    for (auto it = SessionsBegin(); it != SessionsEnd(); ++it)
    {
        if ((*it).second == GetStreamerSession())
        {
            streamer_info = GET_STREAMER_INFO(GetStreamerSession());
            fast_delete(streamer_info);
        }
        else if ((*it).second != GetTrackerSession() &&
                 (*it).second->GetPackedAddr() != GetStun1Addr() &&
                 (*it).second->GetPackedAddr() != GetStun2Addr())
        {
            peer_info = GET_PEER_INFO((*it).second);
            fast_delete(peer_info);
        }
    }
}

void BufferRegister::RegisterPeer(MProtoLib::MSession *session)
{
    PeerInfo *peer_info;
    
    fast_new(peer_info);
    session->SetUserData(peer_info);
    
    SendChunkInfo(session);
    OnPeerConnect(session);
}

void BufferRegister::RegisterStreamer(MSession *session)
{
    StreamerInfo *streamer_info;
    
    fast_new(streamer_info);
    session->SetUserData(streamer_info);
    OnStreamerConnect(session);
}

void BufferRegister::UnRegisterPeer(MSession *session)
{
    PeerInfo *peer_info;
    
    ClearPeerRequest(session);
    OnPeerDisconnect(session);
    peer_info = GET_PEER_INFO(session);
    fast_delete(peer_info);
}

void BufferRegister::UnRegisterStreamer(MSession *session)
{
    StreamerInfo *streamer_info;
    
    ClearStreamerRequest(session);
    OnStreamerDisconnect(session);
    streamer_info = GET_STREAMER_INFO(session);
    fast_delete(streamer_info);
}