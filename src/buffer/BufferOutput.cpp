//
//  BufferOutput.cpp
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferOutput.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

BufferOutput::BufferOutput(CoreObject *core)
:BufferHybridControl(core)
{

}

BufferOutput::~BufferOutput()
{
    ResetBuffer();
}

bool BufferOutput::UpdateBuffer(const uint32_t &chunk_id, const uint8_t *chunk_data)
{
    BufferIterator buf_it;
    uint8_t *copy_data;
    OrderIntIdLess less;

    buf_it = _buffer.find(chunk_id);
    if (buf_it != _buffer.end())
    {
        COErr::Set("Duplicate chunk");
        return false;
    }
    
    if (less(chunk_id,_next_request_id))
    {
        COErr::Set("received chunk is less then next request id");
        return false;
    }
    
    fast_alloc(copy_data, kChunkSize);
    memcpy(copy_data,chunk_data,kChunkSize);
    buf_it = _buffer.begin();
    buf_it = _buffer.insert(buf_it,make_pair(chunk_id, copy_data));
    
    for (;
         buf_it != _buffer.end() && (*buf_it).first == _next_request_id;
         ++buf_it,++_next_request_id)
        OnChunk((*buf_it).first,(*buf_it).second);
    return true;
}

void BufferOutput::ResetBuffer()
{
    _is_next_request_id_set = false;
    for (auto it = _buffer.begin();
         it != _buffer.end();
         ++it)
        fast_free((*it).second);
    
    _buffer.clear();
}

void BufferOutput::ClearBuffer()
{
    StreamerInfo *streamer_info;
    BufferIterator end_it,buf_it;
    
    streamer_info = GET_STREAMER_INFO(GetStreamerSession());
    
    end_it = _buffer.lower_bound((*streamer_info->seconds.begin()));
    
    for (buf_it = _buffer.begin();
         buf_it != end_it;
         buf_it = _buffer.begin())
    {
        fast_free((*buf_it).second);
        _buffer.erase(buf_it);
    }
}