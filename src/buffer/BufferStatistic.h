//
//  BufferStatistic.h
//  Peer
//
//  Created by Oleg on 07.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferStatistic__
#define __Peer__BufferStatistic__

#include "BufferOutput.h"

namespace PeerLib
{
    typedef CoreObjectLib::Event<> StatisticEvent;
    typedef CoreObjectLib::Event
            <const uint64_t&,const uint64_t&,const uint64_t&, const uint64_t &>
    TrackerStatisticEvent;
    
    class BufferStatistic
    :public BufferOutput
    {
    public:
        BufferStatistic(CoreObjectLib::CoreObject *core);
        virtual ~BufferStatistic();
        
        uint64_t    GetCurrentPeerDownload();
        uint64_t    GetCurrentStreamerDownload();
        uint64_t    GetCurrentEconomy();
        uint64_t    GetTotalPeerDownload();
        uint64_t    GetTotalStreamerDownload();
        uint64_t    GetTotalEconomy();
        void        LogStatistic();
        StatisticEvent OnStatistic;
        TrackerStatisticEvent OnTrackerStatistic;
    private:
        CoreObjectLib::Timer *_statistic_timer;
        uint32_t _statistic_timer_id;
        uint64_t _tracker_peer_download,_tracker_streamer_download;
        uint64_t _current_peer_download,_current_streamer_download;
        uint64_t _total_peer_download,_total_streamer_download;
        uint8_t _statistic_data[17];
        
        void StartStatistic();
        void StopStatistic();
        void UpdateStreamerStatistic();
        void UpdatePeerStatistic();
        void CalculateStatistic();
        MPROTO_MESSAGE_PROC(ProcessRepStatistic);
        
        void PeerDeleteStatisticTimer();
        
        friend class Peer;
    };
    
#include "BufferStatistic.inl"
}

#endif /* defined(__Peer__BufferStatistic__) */
