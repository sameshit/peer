//
//  TrackerBase.cpp
//  Peer
//
//  Created by Oleg on 04.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerBase.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace PeerLib;
using namespace std;

TrackerBase::TrackerBase(CoreObject *core)
:MProto(core),_tracker_session(NULL),_streamer_session(NULL),_tracker_state(TrackerNotConnected)
{
    OnConnect.Attach(this,&TrackerBase::ProcessSessionConnected);
    OnDisconnect.Attach(this, &TrackerBase::ProcessSessionDisconnected);
    OnReliableMessage.Attach(this, &TrackerBase::ProcessSessionReliableMessage);
    OnNonReliableMessage.Attach(this, &TrackerBase::ProcessSessionNonReliableMessage);
    
    _connect_tracker_task.OnTimeTask.Attach(this, &TrackerBase::LoopConnectToTracker);
    _connect_tracker_task.SetTimeout(0);
}

TrackerBase::~TrackerBase()
{

}

bool TrackerBase::ConnectToTracker(const char *tracker_ip, const uint16_t &tracker_port, const uint16_t &stun1_port, const char *stun2_ip, const uint16_t &stun2_port)
{
    if (_tracker_state != TrackerNotConnected)
    {
        COErr::Set("Already connected to tracker");
        return false;
    }
 
    _tracker_ip.assign(tracker_ip);
    _tracker_port = tracker_port;
    MSession::MakePackedAddress(&_stun1_addr,tracker_ip,stun1_port);
    MSession::MakePackedAddress(&_stun2_addr,stun2_ip,stun2_port);
    
    _tracker_state = TrackerConnecting;
    
    if (_core->IsLoopThread())
        LoopConnectToTracker();
    else
        _core->GetScheduler()->Add(&_connect_tracker_task);
    
    return true;
}

void TrackerBase::LoopConnectToTracker()
{
    
    Connect(_tracker_ip.c_str(), _tracker_port);
}

void TrackerBase::ProcessSessionConnected(MSession *session)
{
    if (_tracker_state == TrackerNotConnected)
    {
        session->GetDisconnectReason()<<"Someone connected while not connected to tracker";
        Disconnect(session);
        return;
    }
    
    if (_tracker_state == TrackerConnecting)
    {
        _tracker_state = TrackerConnected;
        _tracker_session = session;
        SendTrackerProtocolVersion();
        ProcessLocalNatInfo(session);
        OnTrackerConnect(session);
        return;
    }
    
    if (session->GetPackedAddr() == _stun1_addr || session->GetPackedAddr() == _stun2_addr)
    {
        StunConnected(session);
        return;
    }

    if (_tracker_state == TrackerPlayingAndConnectingToStreamer)
    {
        _tracker_state = TrackerPlaying;
        _streamer_session = session;
        RegisterStreamer(session);
        BeginPlay();
        OnStreamerConnect(session);
        return;
    }
    
    if (_tracker_state != TrackerPlaying)
    {
        session->GetDisconnectReason() << "Someone connected while not playing any stream";
        Disconnect(session);
        return;
    }
    
    RegisterPeer(session);
    OnPeerConnect(session);
}

void TrackerBase::ProcessSessionDisconnected(MSession *session)
{
    if (_tracker_session == session)
    {
        DisconnectAllNeighbors("Tracker connection lost");
        _tracker_state = TrackerNotConnected;
        OnTrackerDisconnect(session);
        _tracker_session = NULL;
        return;
    }
    
    if (_streamer_session == session)
    {
        DisconnectAllNeighbors("Streamer connection lost");
        if (_tracker_state != TrackerNotConnected)
            _tracker_state = TrackerConnected;
        EndPlay();
        UnRegisterStreamer(session);
        OnStreamerDisconnect(session);
        _streamer_session = NULL;
        return;
    }
    
    if (session->GetPackedAddr() == _stun1_addr || session->GetPackedAddr() == _stun2_addr)
    {
        StunDisconnected(session);
        return;
    }
    
    if (_tracker_state == TrackerPlaying)
    {
        UnRegisterPeer(session);
        OnPeerDisconnect(session);
        return;
    }
}

void TrackerBase::ProcessSessionReliableMessage(MSession *session, uint8_t *data, ssize_t size)
{
    if (session == _tracker_session)
        ProcessTrackerReliableMessage(session,data,size);
    else
    {
        session->GetDisconnectReason() << "Received reliable message";
        Disconnect(session);
    }
}

void TrackerBase::ProcessSessionNonReliableMessage(MSession *session, uint8_t *data, ssize_t size)
{
    if (session == _tracker_session)
    {
        session->GetDisconnectReason() << "Processing non reliable message from tracker!";
        Disconnect(session);        
    }
    else if (session == _streamer_session)
        ProcessStreamerNonReliable(session,data,size);
    else
        ProcessPeerNonReliable(session,data,size);
}

void TrackerBase::DisconnectAllNeighbors(const char *reason)
{
    MSessionMapIterator it,next_it;
    
    for (it = SessionsBegin(); it != SessionsEnd(); it = SessionsBegin())
    {
        if ((*it).second == _tracker_session)
        {
             if (SessionsSize() == 1)
                 break;
             else
                 ++it;
        }
        (*it).second->GetDisconnectReason() << reason;
        Disconnect(it);
    }
}