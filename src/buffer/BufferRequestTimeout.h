//
//  BufferRequestTimeout.h
//  Peer
//
//  Created by Oleg on 04.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferRequestTimeout__
#define __Peer__BufferRequestTimeout__

#include "BufferCongestion.h"

namespace PeerLib
{
    class BufferRequestTimeout
    :public BufferCongestion
    {
    public:
        BufferRequestTimeout(CoreObjectLib::CoreObject *core);
        virtual ~BufferRequestTimeout();
    private:
        CoreObjectLib::Timer *_request_timeout_timer;
    
        void CheckAllRequestsTimeout();
        void PeerDeleteRequestTimer();
        friend class Peer;
    };
}

#endif /* defined(__Peer__BufferRequestTimeout__) */
