//
//  BufferHybridControl.cpp
//  Peer
//
//  Created by Oleg on 15.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferHybridControl.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

BufferHybridControl::BufferHybridControl(CoreObject *core)
:BufferRequestTimeout(core),_is_super_peer(false)
{
    fast_new(_hybrid_control_timer,_core,200);
    _hybrid_control_timer->OnTimer.Attach(this, &BufferHybridControl::HybridControl);    
    _hybrid_control_task.OnTimeTask.Attach(this, &BufferHybridControl::ProcessTryHybridControl);
    OnCloseStream.Attach(this, &BufferHybridControl::ResetSuperPeer);
}

BufferHybridControl::~BufferHybridControl()
{
    fast_delete(_hybrid_control_timer);
}

ssize_t BufferHybridControl::GetBufferInSeconds()
{
    StreamerInfo *streamer_info;
    BufferIterator buf_it1,buf_it2;
    ChunkInfoIterator sec_it1,sec_it2;
    
    streamer_info = GET_STREAMER_INFO(GetStreamerSession());
    
    buf_it1 = _buffer.begin();
    buf_it2 = _buffer.find(_next_request_id - 1);
    
    if (buf_it1 == _buffer.end() || buf_it2 == _buffer.end())
        return 0;
    
    sec_it1 = streamer_info->seconds.lower_bound((*buf_it1).first);
    sec_it2 = streamer_info->seconds.lower_bound((*buf_it2).first);
    
    if (sec_it1 == streamer_info->seconds.end() || sec_it2 == streamer_info->seconds.end())
        return 0;
    
    return distance(sec_it1, sec_it2);
}

void BufferHybridControl::HybridControl()
{
    ssize_t seconds;
    StreamerInfo *streamer_info;
    
    if (GetTrackerState() != TrackerPlaying)
        return;
    streamer_info = GET_STREAMER_INFO(GetStreamerSession());
    
    
    seconds = GetBufferInSeconds();
    if (streamer_info->is_streamer_downloading)
    {
        if (seconds > 6 && !_is_super_peer)
        {
            streamer_info->is_streamer_downloading = false;
            if (PeerSettings()->log_hybrid_control)
                LOG_VERBOSE("Downloading from streamer is stopped. Buffer size is "<<seconds);
        }
    }
    else
    {
        if (seconds < 4 || _is_super_peer)
        {
            streamer_info->is_streamer_downloading = true;
            if (PeerSettings()->log_hybrid_control)
                LOG_VERBOSE("Started download from streamer. Buffer size is "<<seconds);
            CongestionControl(GetStreamerSession(),
                              &streamer_info->total_request_info,
                              &streamer_info->total_request_info.request1,
                              CongestionReset);
            CongestionControl(GetStreamerSession(),
                              &streamer_info->total_request_info,
                              &streamer_info->total_request_info.request2,
                              CongestionReset);
        }
    }
}

bool is_set = false;

void BufferHybridControl::TryHybridControl()
{
/*    if (is_set)
        return;
    _hybrid_control_task.SetTimeout(rand() % 2000);
    _core->GetScheduler()->Add(&_hybrid_control_task);
    is_set = true;*/
    
    
/*    if (rand() % 2 == 0 || GetBufferInSeconds() < 3)*/
    ProcessTryHybridControl();
}

void BufferHybridControl::ProcessTryHybridControl()
{
    StreamerInfo *streamer_info;
    
    streamer_info = GET_STREAMER_INFO(GetStreamerSession());
    TryRequest(GetStreamerSession(), &streamer_info->total_request_info);
    is_set = false;
}

void BufferHybridControl::ProcessRepSuperPeerEnable(MSession *session, uint8_t *data, ssize_t size)
{
    if (size != 0)
    {
        session->GetDisconnectReason() << "Invalid 'Super peer enable' size("<<size<<")";
        Disconnect(session);
        return;
    }
    
    if (_is_super_peer)
    {
        session->GetDisconnectReason() << "Received enable super peer message, while already super peer";
        Disconnect(session);
        return;
    }
    
    if (PeerSettings()->log_super_peer)
        LOG_VERBOSE("I'm super peer from now");
    
    _is_super_peer = true;
    HybridControl();
}

void BufferHybridControl::ProcessRepSuperPeerDisable(MSession *session, uint8_t *data, ssize_t size)
{
    if (size != 0)
    {
        session->GetDisconnectReason() << "Invalid 'Super peer disable' size("<<size<<")";
        Disconnect(session);
        return;
    }
    
    if (!_is_super_peer)
    {
        session->GetDisconnectReason() << "Received enable super peer message, while already is not super peer";
        Disconnect(session);
        return;
    }
    
    if (PeerSettings()->log_super_peer)
        LOG_VERBOSE("I'm not super peer from now");
    
    _is_super_peer = false;
    HybridControl();
}

void BufferHybridControl::ResetSuperPeer()
{
    _is_super_peer = false;
}