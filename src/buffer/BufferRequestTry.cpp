//
//  BufferInfo.cpp
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferRequestTry.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

BufferRequestTry::BufferRequestTry(CoreObject *core)
:BufferChunkInfo(core),_is_next_request_id_set(false)
{

}

BufferRequestTry::~BufferRequestTry()
{

}

void BufferRequestTry::TryRequest(MSession *session,TotalRequestInfo *total_request_info)
{
    ChunkInfoIterator seconds_it;
    OrderIntIdLess less;
    RequestInfo *request_info;
    CongestionState congestion_state;
    StreamerInfo *streamer_info;
    BufferIterator buf_it;
    
    streamer_info = GET_STREAMER_INFO(GetStreamerSession());
    congestion_state = CongestionReset;
    
    if (streamer_info->seconds.empty())
        return;
    
    if (!_is_next_request_id_set || less(_next_request_id,(*streamer_info->seconds.begin())))
    {
        seconds_it = streamer_info->seconds.begin();
        for (auto i = streamer_info->seconds.size() / 2;
             i > 0;
             --i)
            ++seconds_it;
        
        _next_request_id        = (*seconds_it);
        for (buf_it = _buffer.find(_next_request_id);buf_it != _buffer.end() && (*buf_it).first == _next_request_id; ++ buf_it)
            ++_next_request_id;
        _is_next_request_id_set = true;
//        LOG "NEXT REQUEST ID RESET!"EL;
            
        request_info = &total_request_info->request1;
        CongestionControl(session,total_request_info,request_info,congestion_state);
        request_info = &total_request_info->request2;
        CongestionControl(session,total_request_info,request_info,congestion_state);
    }
    else
    {
        request_info = &total_request_info->request1;
        if (!request_info->requested)
            CongestionControl(session,total_request_info,request_info,congestion_state);
        request_info = &total_request_info->request2;
        if (!request_info->requested)
            CongestionControl(session,total_request_info,request_info,congestion_state);
    }
}

bool BufferRequestTry::UpdateRequest(MSession* session,TotalRequestInfo *total_request_info,const uint32_t& chunk_id)
{
    CongestionState congestion_state;
    ChunkInfoIterator request_it1,request_it2;
    
    if (!_is_next_request_id_set)
    {
        if (PeerSettings()->log_skipped_chunks)
        {
            LOG_VERBOSE("Skipped chunk("<<chunk_id<<") from "<<PRINT_SESSION_ADDR(session)
            <<" because next request id is not set");
        }
        return false;
    }
    request_it1 = total_request_info->request1.requested_chunks.find(chunk_id);
    request_it2 = total_request_info->request2.requested_chunks.find(chunk_id);
    
    if (request_it1 == total_request_info->request1.requested_chunks.end() &&
        request_it2 == total_request_info->request2.requested_chunks.end())
    {
        if (PeerSettings()->log_skipped_chunks)
        {
            LOG_VERBOSE("Skipped chunk("<<chunk_id<<") from "<<PRINT_SESSION_ADDR(session)<<
            " because it wasn't reqeusted.");
        }
        
        return false;
    }
    
    if (total_request_info->is_first_request)
    {
        if (request_it1 == total_request_info->request1.requested_chunks.end())
        {
            total_request_info->request2.responsed_chunks.insert(chunk_id);
            congestion_state = CongestionDecrease;
            CongestionControl(session,total_request_info,&total_request_info->request1,congestion_state);
        }
        else
        {
            total_request_info->request1.responsed_chunks.insert(chunk_id);
            if (total_request_info->request1.responsed_chunks.size()
                == total_request_info->request1.requested_chunks.size())
            {
                congestion_state = CongestionIncrease;
                CongestionControl(session,total_request_info,&total_request_info->request1,congestion_state);
            }
        }
    }
    else
    {
        if (request_it2 == total_request_info->request2.requested_chunks.end())
        {
            total_request_info->request1.responsed_chunks.insert(chunk_id);
            congestion_state = CongestionDecrease;
            CongestionControl(session,total_request_info,&total_request_info->request2,congestion_state);
        }
        else
        {
            total_request_info->request2.responsed_chunks.insert(chunk_id);
            if (total_request_info->request2.responsed_chunks.size()
                == total_request_info->request2.requested_chunks.size())
            {
                congestion_state = CongestionIncrease;
                CongestionControl(session,total_request_info,&total_request_info->request2,congestion_state);
            }
        }
    }
    return true;
}