//
//  BufferDeserialize.cpp
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferDeserialize.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

BufferDeserialize::BufferDeserialize(CoreObject *core)
:BufferRecv(core)
{

}

BufferDeserialize::~BufferDeserialize()
{
    
}

void BufferDeserialize::DeserializePeerChunkInfo(MProtoLib::MSession *session, uint8_t *data, ssize_t size)
{
    /* message format:
    
      first_id (4 bytes)
      last_id (4 bytes)
      chunk_info (last bytes)
    */
    uint8_t *pos;
    ChunkInfo seconds,last_second;
    PeerInfo *peer_info;
    ssize_t remaining_size,i;
    uint32_t chunk_id;
    bool if_chunk_set;
    UnOrderedChunkInfoIterator insert_it;
    
    if (size < 8)
    {
        session->GetDisconnectReason() << "Invalid size("<<size<<
            ") while processing peer chunk info message";
        Disconnect(session);
    }
    
    peer_info = GET_PEER_INFO(session);
    peer_info->chunk_info.clear();
    peer_info->first_id = Utils::GetBe32(data);
    peer_info->last_id = Utils::GetBe32(data + 4);
    
    insert_it = peer_info->chunk_info.begin();
    remaining_size = size - 8;
    if (remaining_size >= 0)
    {        
        chunk_id = peer_info->last_id;
        pos = data + 8;
        i = 0;
        
        while (remaining_size > 0)
        {
            while (remaining_size > 0 && !if_chunk_set)
            {
                if_chunk_set = *pos & (1 << (i%8));
                ++i;
                ++chunk_id;
                
                if (i == 8)
                {
                    --remaining_size;
                    i = 0;
                    pos += 1;
                }
            }
            if (remaining_size > 0)
            {
                insert_it = peer_info->chunk_info.insert(insert_it,chunk_id);
                if_chunk_set = false;
            }
        }
    }
    
    if (PeerSettings()->log_peer_chunk_info)
    {
        LOG_VERBOSE("Received chunk info from peer "<<PRINT_SESSION_ADDR(session)<<"."<<endl
        << "First id: "<<peer_info->first_id<<", last id: "<<peer_info->last_id<<endl
        << "Size of chunk info: "<<peer_info->chunk_info.size());
    }
    
    TryRequest(session,&peer_info->total_request_info);
}

void BufferDeserialize::DeserializePeerRequest(MSession *session, uint8_t *data, ssize_t size)
{
    PeerMessageType type;
    uint32_t chunk_id;
    uint8_t *pos;
    ssize_t remaining_size,i;
    bool if_chunk_set;
    ChunkInfo request;
    ChunkInfoIterator request_it;
    
    if (size < 4)
    {
        session->GetDisconnectReason() << "Invalid size("<<size<<") of Request message.";
        Disconnect(session);
        return;
    }
    
    type = PeerMessageResponse;
    chunk_id = Utils::GetBe32(data);
    pos = data + 4;
    remaining_size = size - 4;
    i = 0;
    request_it = request.begin();
    
    do
    {
        request_it = request.insert(request_it,chunk_id);
        
        if_chunk_set = false;
        while (remaining_size > 0 && !if_chunk_set)
        {
            if_chunk_set = *pos & (1 << (i%8));
            ++i;
            ++chunk_id;
            
            if (i == 8)
            {
                --remaining_size;
                i = 0;
                pos += 1;
            }
        }
    }
    while (remaining_size > 0);
    if (PeerSettings()->log_incoming_requests)
    {
        LOG_VERBOSE( "Received request from "<<PRINT_SESSION_ADDR(session)<<"."<<endl
        << "Size of request: "<<request.size()<<"."<<endl
        << "Data: ");
        
        for (auto it = request.begin();
             it != request.end();
             ++it)
            cout << (*it)<< " ";
        cout<<endl;
    }
    
    ProcessPeerRequest(session,request);
}

void BufferDeserialize::DeserializeStreamerChunkInfo(MSession *session, uint8_t *data, ssize_t size)
{
    /* message format:
     seconds (at least 8 bytes)
    */
    StreamerInfo *streamer_info;
    ChunkInfoIterator seconds_it;
    
    if (size < 8 || size % 4 != 0)
    {
        session->GetDisconnectReason()<<"Invalid size("<<size<<") of streamer chunk info";
        Disconnect(session);
        return;
    }
    
    streamer_info = GET_STREAMER_INFO(session);
    streamer_info->seconds.clear();
    seconds_it = streamer_info->seconds.begin();
    for (ssize_t i = 0; i < size; i+=4)
        seconds_it = streamer_info->seconds.insert(seconds_it,Utils::GetBe32(data + i));
    
    if (PeerSettings()->log_streamer_chunk_info)
    {
        LOG_VERBOSE( "Received chunk info from streamer. Size: "<<streamer_info->seconds.size()
        << ", first id: "<<Utils::GetBe32(data)<<", last id: "
        <<Utils::GetBe32(data+(size-4)) );
    }
    
    ClearRequests();
    ClearBuffer();
    SendChunkInfoToAll();
    TryHybridControl();
//    TryRequest(session,&streamer_info->total_request_info);
}

void BufferDeserialize::DeserializeResponse(MSession *session, uint8_t *data, ssize_t size)
{
    uint32_t chunk_id;
    
    if (size != kChunkSize + 4)
    {
        session->GetDisconnectReason()<<"Invalid size("<<size<<") of response";
        Disconnect(session);
        return;
    }
    
    chunk_id = Utils::GetBe32(data);
    ProcessResponse(session,chunk_id, data+4);
}