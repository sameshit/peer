//
//  BufferRegister.h
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferRegister__
#define __Peer__BufferRegister__

#include "BufferBase.h"

namespace PeerLib
{
    class BufferRegister
    :public BufferBase
    {
    public:
        BufferRegister(CoreObjectLib::CoreObject *core);
        virtual ~BufferRegister();
    protected:
        MPROTO_CONNECT_VPROC(SendChunkInfo);
        MPROTO_DISCONNECT_VPROC(ClearPeerRequest);
        MPROTO_DISCONNECT_VPROC(ClearStreamerRequest);
    private:
        MPROTO_CONNECT_PROC(RegisterPeer);
        MPROTO_DISCONNECT_PROC(UnRegisterPeer);
        MPROTO_CONNECT_PROC(RegisterStreamer);
        MPROTO_DISCONNECT_PROC(UnRegisterStreamer);
    };
    
    enum PeerState
    {
        PeerNotAuthed,
        PeerAuthed,
    };
    
    class RequestInfo
    {
    public:
        ChunkInfo               requested_chunks,responsed_chunks;
        bool                    fast_start;
        bool                    requested;
        CoreObjectLib::COTime   request_time;
        uint32_t                max_request_size;
        
        RequestInfo():fast_start(true),requested(false) {}
        virtual ~RequestInfo() {}
    };
    
    class TotalRequestInfo
    {
    public:
        RequestInfo request1;
        RequestInfo request2;
        bool        is_first_request;        
        
        TotalRequestInfo():is_first_request(true){}
        virtual ~TotalRequestInfo() {}
    };
    
    class PeerInfo
    {
    public:
        PeerState           peer_state;
        uint64_t            real_addr;
        UnOrderedChunkInfo           chunk_info;
        uint32_t            first_id,last_id;
        TotalRequestInfo    total_request_info;        
        
        PeerInfo():peer_state(PeerNotAuthed) {}
        virtual ~PeerInfo() {}
    };
    
    class StreamerInfo
    {
    public:
        bool                is_streamer_downloading;
        ChunkInfo           seconds;
        TotalRequestInfo    total_request_info;
        
        StreamerInfo()
        :is_streamer_downloading(true){}
        virtual ~StreamerInfo() {}
    };
    
#define GET_PEER_INFO(ses)      ((PeerInfo*)ses->GetUserData())
#define GET_STREAMER_INFO(ses)  ((StreamerInfo*)ses->GetUserData())
}

#endif /* defined(__Peer__BufferRegister__) */
