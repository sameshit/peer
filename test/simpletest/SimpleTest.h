//
//  SimpleTest.h
//  Peer
//
//  Created by Oleg on 29.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__SimpleTest__
#define __Peer__SimpleTest__

#include "../../src/Peer.h"

namespace PeerLib
{
    class SimpleTest
    :public Peer
    {
    public:
        SimpleTest(CoreObjectLib::CoreObject *core,const uint16_t &port);
        virtual ~SimpleTest();
        
        void Run();
    private:
        CoreObjectLib::TimeTask _reconnect_task;
        uint16_t _port;
        
        void ParseChunk(const uint32_t &chunk_id,const uint8_t *chunk) {LOG_VERBOSE(chunk_id);}
        
        MPROTO_CONNECT_PROC(TrackerConnected);
        MPROTO_CONNECT_PROC(TrackerDisconnected);
        MPROTO_CONNECT_PROC(StreamerConnected);
        MPROTO_CONNECT_PROC(StreamerDisconnected);
        MPROTO_CONNECT_PROC(PeerConnected);
        MPROTO_CONNECT_PROC(PeerDisconnected);
        void StreamPlayed();
        void StreamClosed();
    };
}

#endif /* defined(__Peer__SimpleTest__) */
