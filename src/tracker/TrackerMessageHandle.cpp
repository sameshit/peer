//
//  TrackerMessageHandle.cpp
//  Peer
//
//  Created by Oleg on 04.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerMessageHandle.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace PeerLib;
using namespace std;

TrackerMessageHandle::TrackerMessageHandle(CoreObject *core)
:TrackerPeerFunctions(core)
{

}

TrackerMessageHandle::~TrackerMessageHandle()
{

}

void TrackerMessageHandle::ProcessRepAllStreams(MSession *session, uint8_t *data, ssize_t size)
{
    uint64_t stream_id;
    uint8_t *pos;
    ssize_t i;
    set<uint64_t> stream_set;
    
    if (size % 8 != 0)
    {
        _tracker_session->GetDisconnectReason() << "Invalid AllStreams message size("<<size<<")";
        Disconnect(_tracker_session);
        return;
    }
    
    for (i = 0,pos = data; i < size; i+=8, pos += 8)
    {
        stream_id = Utils::GetBe64(pos);
        stream_set.insert(stream_id);
    }
    
    OnAllStreams(stream_set);
}

void TrackerMessageHandle::ProcessRepStreamClosed(MSession *session, uint8_t *data, ssize_t size)
{
    _tracker_state = TrackerConnected;
    DisconnectAllNeighbors("Stream closed by tracker");
    OnCloseStream();
}
void TrackerMessageHandle::ProcessRepNeighborChange(MSession *session,uint8_t *data, ssize_t size)
{
    struct sockaddr_in addr;
    uint8_t *pos;
    ssize_t i;
    bool need_disconnect;
    MSessionMapIterator ses_it;
    MSession *session2;
    
    if (_tracker_state != TrackerPlaying)
    {
        _tracker_session->GetDisconnectReason() << "Invalid tracker state("<<_tracker_state<<") while processing GetPeers message";
        Disconnect(_tracker_session);
        return;
    }
    
    if (size < 9 || (size-1) % 8 != 0)
    {
        _tracker_session->GetDisconnectReason() << "Invalid message size("<<size<<") in ProcessGetPeers";
        Disconnect(_tracker_session);
        return;
    }
    
    need_disconnect = data[0] == 1;
    if (need_disconnect)
    {
        size-=9;
        ses_it = FindSession(*(uint64_t*)(data+size+1));
        if (ses_it != SessionsEnd())
        {
            session2 = (*ses_it).second;
            session2->GetDisconnectReason() << "Neighbor change command received";
            Disconnect(session2);
        }
        return;
    }
    else
        size-=1;
    
    for (i = 0,pos = data+1; i < size; i+=8, pos += 8)
    {
        MSession::UnPackAddress(pos, &addr);
        Connect(&addr);
    }
}

void TrackerMessageHandle::ProcessRepRealAddr(MSession *session,uint8_t *data, ssize_t size)
{
    if (size != 8)
    {
        _tracker_session->GetDisconnectReason() <<  "Invalid header size("<< size <<") in ProcessRealAddr" ;
        Disconnect(_tracker_session);
        return;
    }
    
    memcpy((void*)&_real_addr,data,8);
}

void TrackerMessageHandle::ProcessRepStreamer(MSession *session, uint8_t *data, ssize_t size)
{
    struct sockaddr_in addr;
    
    if (size != 8)
    {
        _tracker_session->GetDisconnectReason() << "Invalid size("<<size<<") while processing RepStreamer message";
        Disconnect(_tracker_session);
        return;
    }
    
    if (_tracker_state != TrackerPlayingAndWaitingForStreamer)
    {
        _tracker_session->GetDisconnectReason() <<"Invalid tracker state("<<_tracker_state<<") while processing RepStreamer message";
        Disconnect(_tracker_session);
        return;
    }
    
    MSession::UnPackAddress(data, &addr);
    _tracker_state = TrackerPlayingAndConnectingToStreamer;
    Connect(&addr);
}