//
//  BufferOutput.h
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferOutput__
#define __Peer__BufferOutput__

#include "BufferHybridControl.h"

typedef CoreObjectLib::Event<const uint32_t &,const uint8_t *> ChunkEvent;

namespace PeerLib
{
    class BufferOutput
    :public BufferHybridControl
    {
    public:
        BufferOutput(CoreObjectLib::CoreObject *core);
        virtual ~BufferOutput();
        
        ChunkEvent OnChunk;
    private:
        bool UpdateBuffer(const uint32_t &chunk_id,const uint8_t *data);
        void ResetBuffer();
        void ClearBuffer();
    };
}

#endif /* defined(__Peer__BufferOutput__) */
