//
//  TrackerBase.h
//  Peer
//
//  Created by Oleg on 04.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__TrackerBase__
#define __Peer__TrackerBase__

#include "../../../MProto/src/MProto.h"

namespace PeerLib
{
    enum TrackerState
    {
        TrackerNotConnected,
        TrackerConnecting,
        TrackerConnected,
        TrackerPlayingAndWaitingForStreamer,
        TrackerPlayingAndConnectingToStreamer,
        TrackerPlaying,
    };
    
    typedef CoreObjectLib::Event<> StreamerDisconnectEvent;
    
    class TrackerBase
    :public MProtoLib::MProto
    {
    public:
        TrackerBase(CoreObjectLib::CoreObject *core);
        virtual ~TrackerBase();
        
        bool                        ConnectToTracker(const char *tracker_ip, const uint16_t &tracker_port,
                                                     const uint16_t &stun1_port, const char *stun2_ip,
                                                     const uint16_t &stun2_port);
        inline bool                 ConnectToTracker(const std::string &tracker_ip,const uint16_t &tracker_port,
                                                     const uint16_t &stun1_port,const std::string &stun2_ip,
                                                     const uint16_t &stun2_port)
                                    {return ConnectToTracker(tracker_ip.c_str(),
                                                             tracker_port,stun1_port,
                                                             stun2_ip.c_str(),
                                                             stun2_port);}
        MProtoLib::MSession *GetStreamerSession() {return _streamer_session;}
        MProtoLib::MSession *GetTrackerSession() {return _tracker_session;}
        const TrackerState& GetTrackerState() {return _tracker_state;}
        
        MProtoLib::ConnectEvent     OnTrackerConnect,OnPeerConnect,OnStreamerConnect;
        MProtoLib::DisconnectEvent  OnTrackerDisconnect,OnPeerDisconnect,OnStreamerDisconnect;
    protected:
        MPROTO_CONNECT_VPROC(RegisterStreamer);
        MPROTO_CONNECT_VPROC(RegisterPeer);
        MPROTO_DISCONNECT_VPROC(UnRegisterStreamer);
        MPROTO_DISCONNECT_VPROC(UnRegisterPeer);
        MPROTO_MESSAGE_VPROC(ProcessPeerNonReliable);
        MPROTO_MESSAGE_VPROC(ProcessStreamerNonReliable);
        MPROTO_MESSAGE_VPROC(ProcessTrackerReliableMessage);
        MPROTO_CONNECT_VPROC(ProcessLocalNatInfo);
        MPROTO_CONNECT_VPROC(StunConnected);
        MPROTO_DISCONNECT_VPROC(StunDisconnected);
        virtual void BeginPlay() = 0;
        virtual void EndPlay() = 0;
        virtual void SendTrackerProtocolVersion() = 0;
        
        inline const uint64_t& GetStun1Addr() {return _stun1_addr;}
        inline const uint64_t& GetStun2Addr() {return _stun2_addr;}
    private:
        std::string _tracker_ip;
        uint16_t    _tracker_port;
        MProtoLib::MSession *_tracker_session,*_streamer_session;
        TrackerState _tracker_state;
        CoreObjectLib::TimeTask _connect_tracker_task;
        uint64_t _stun1_addr,_stun2_addr;
        
        MPROTO_CONNECT_PROC(ProcessSessionConnected);
        MPROTO_DISCONNECT_PROC(ProcessSessionDisconnected);
        MPROTO_MESSAGE_PROC(ProcessSessionReliableMessage);
        MPROTO_MESSAGE_PROC(ProcessSessionNonReliableMessage);
        void    DisconnectAllNeighbors(const char *);
        void    LoopConnectToTracker();
        
        friend class TrackerStun;
        friend class TrackerRecv;
        friend class TrackerPeerFunctions;
        friend class TrackerMessageHandle;
    };    
}

#endif /* defined(__Peer__TrackerBase__) */
