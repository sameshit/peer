//
//  BufferPeerProcess.cpp
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferProcess.h"

using namespace PeerLib;
using namespace MProtoLib;
using namespace CoreObjectLib;
using namespace std;

BufferProcess::BufferProcess(CoreObject *core)
:BufferDeserialize(core)
{

}

BufferProcess::~BufferProcess()
{
    
}

void BufferProcess::ProcessPeerRequest(MSession *session, const ChunkInfo &request)
{
    PeerMessageType type;
    uint8_t be_id[4];
    
    type = PeerMessageResponse;
    for (auto it = request.begin(); it != request.end(); ++it)
    {
        auto buf_it = _buffer.find((*it));
        if (buf_it == _buffer.end())
        {
            if (PeerSettings()->log_unsuccessful_requests)
                LOG_VERBOSE("Requested chunk("<<(*it)<<") from "<<PRINT_SESSION_ADDR(session)<<
                    " that I don't have. Skipping...");
        }
        else
        {
            Utils::PutBe32(be_id, (*buf_it).first);
            SendNonReliable(session, (uint8_t*)&type, 1,be_id,4,(*buf_it).second,kChunkSize);
        }
    }
}

void BufferProcess::ProcessResponse(MSession *session, const uint32_t &chunk_id,
                                        const uint8_t *chunk_data)
{
    TotalRequestInfo *total_request_info;
    
    if (session == GetStreamerSession())
    {
        total_request_info = &GET_STREAMER_INFO(session)->total_request_info;
        if (PeerSettings()->log_streamer_responses)
            LOG_VERBOSE("Received chunk("<<chunk_id<<") from streamer");
    }
    else
    {
        total_request_info = &GET_PEER_INFO(session)->total_request_info;
        if (PeerSettings()->log_peer_responses)
            LOG_VERBOSE("Received chunk("<<chunk_id<<") from "<<PRINT_SESSION_ADDR(session));
    }
    
    if (UpdateRequest(session,total_request_info,chunk_id))
    {
        if (UpdateBuffer(chunk_id, chunk_data))
        {
            if (session == GetStreamerSession())
                UpdateStreamerStatistic();
            else
                UpdatePeerStatistic();
            UpdateChunkInfoCounter();
        }
        else if (PeerSettings()->log_unsuccesful_responses)
            LOG_VERBOSE( "Couldn't update buffer with chunk("<<chunk_id<<"). Reason: "<<COErr::Get() );
    }
}