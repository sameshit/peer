//
//  BufferStatistic.cpp
//  Peer
//
//  Created by Oleg on 07.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferStatistic.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

BufferStatistic::BufferStatistic(CoreObject *core)
:BufferOutput(core),_tracker_peer_download(0),_tracker_streamer_download(0)
{
    fast_new(_statistic_timer,_core,1000);
    Utils::PutByte(_statistic_data,MTReqStatistic);
}

BufferStatistic::~BufferStatistic()
{
    PeerDeleteStatisticTimer();
}

void BufferStatistic::CalculateStatistic()
{
    _total_peer_download += _current_peer_download;
    _total_streamer_download += _current_streamer_download;
    
    if (PeerSettings()->log_statistic)
        LogStatistic();
    
    OnStatistic();
    _current_peer_download = 0;
    _current_streamer_download = 0;
}

void BufferStatistic::PeerDeleteStatisticTimer()
{
    if (_statistic_timer != NULL)
    {
        fast_delete(_statistic_timer);
        _statistic_timer = NULL;
    }
}

void BufferStatistic::ProcessRepStatistic(MSession *session, uint8_t *data, ssize_t size)
{
    uint64_t peers_count,streamer_download,peer_download,eco;

    if (size != 24)
    {
        session->GetDisconnectReason() << "Invalid size("<<size<<") of RepStatistic";
        Disconnect(session);
        return;
    }
    
    peers_count = Utils::GetBe64(data);
    streamer_download = Utils::GetBe64(data+8);
    peer_download = Utils::GetBe64(data+16);
    if (streamer_download+peer_download == 0)
        eco = 0;
    else
        eco = (peer_download*100)/(streamer_download+peer_download);
    
    if (PeerSettings()->log_statistic)
    {
        LOG_VERBOSE( "Total viewers: "<<peers_count<<endl<<
        "Transfered from streamer: "<<streamer_download/1024<<"KB"<<endl<<
        "Transfered from peers: "<<peer_download/1024<<"KB"<<endl<<
        "Economy: "<<eco<<"%" );
        
    }
    
    OnTrackerStatistic(peers_count,streamer_download,peer_download,eco);
    
    Utils::PutBe64(_statistic_data+1, _tracker_streamer_download);
    Utils::PutBe64(_statistic_data+9, _tracker_peer_download);
    SendReliable(session, _statistic_data, 17);
    
    _tracker_peer_download = 0;
    _tracker_streamer_download = 0;
}