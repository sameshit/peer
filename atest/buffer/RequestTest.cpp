//
//  RequestTest.cpp
//  Peer
//
//  Created by Oleg on 22.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "../../src/buffer/BufferManager.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

#define RESET     buffer_manager->Reset(); \
seconds.clear(); \
request.clear(); \
chunk_info.clear();

ATEST(SimpleRequestTest)
{
    CoreObject *_core;
    BufferManager *buffer_manager;
    ChunkInfo request,chunk_info,seconds;
    ChunkInfoIterator it;
    OrderIntIdLess less;
    
    _core = new CoreObject(true);
    fast_new1(BufferManager, buffer_manager, _core);
    
    // 1. Simple streamer request should be empty when no streamer chunk info provided
    buffer_manager->GenerateStreamerRequest(10, &request);
    ATEST_EQUAL(request.size(), 0);
    RESET
    
    // 2. Test the simpliest case of streamer request
    for (int i = 0 ; i < 10; ++i)
        seconds.insert(i);
    buffer_manager->UpdateStreamerChunkInfo(0, 10, 5, seconds);
    buffer_manager->GenerateStreamerRequest(10, &request);
    ATEST_EQUAL(request.size(), 6);
    RESET

    
    // 3. Simple request with random vals
    for (int i =0 ; i < 10; i+=3)
        seconds.insert(i);
    buffer_manager->UpdateStreamerChunkInfo(0, 15, 5, seconds);
    buffer_manager->GenerateStreamerRequest(5, &request);
    ATEST_EQUAL(request.size(), 5); it = request.begin();
    ATEST_EQUAL((*it), 5); ++it;
    ATEST_EQUAL((*it), 6); ++it;
    ATEST_EQUAL((*it), 7); ++it;
    RESET
    
    // 4. Simple request of streamer and peer
    for (int i =0; i < 10; ++i)
    {
        seconds.insert(i);
        chunk_info.insert(i);
    }
    buffer_manager->UpdateStreamerChunkInfo(0, 10, 5, seconds);
    buffer_manager->GenerateStreamerRequest(3, &request);
    ATEST_EQUAL(request.size(), 3); it = request.begin();
    ATEST_EQUAL((*it), 6); ++it; ++it;
    ATEST_EQUAL((*it), 8);
    request.clear();
    buffer_manager->GeneratePeerRequest(2, chunk_info, &request);
    ATEST_EQUAL(request.size(), 1);
    RESET
    
    // 5. Request with negative values
    for (int i = 0 - 10; less(i,10); ++i)
    {
        seconds.insert(i);
        chunk_info.insert(i);
    }
    buffer_manager->UpdateStreamerChunkInfo(0-10, 10, 0-5, seconds);
    buffer_manager->GenerateStreamerRequest(3, &request);
    ATEST_EQUAL(request.size(), 3); it = request.begin();
    ATEST_EQUAL((*it), 0-4); ++it; ++it;
    ATEST_EQUAL((*it), 0-2);
    request.clear();
    buffer_manager->GeneratePeerRequest(2, chunk_info, &request);
    ATEST_EQUAL(request.size(), 2); it = request.begin();
    ATEST_EQUAL((*it), 0-1); ++ it;
    ATEST_EQUAL((*it),0);
    RESET
    
    // 6. Request with negative values 2
    for (int i = 0 - 5; less(i,10); ++i)
    {
        seconds.insert(i);
        chunk_info.insert(i);
    }
    buffer_manager->UpdateStreamerChunkInfo(0-5, 10, 0, seconds);
    buffer_manager->GenerateStreamerRequest(3, &request);
    ATEST_EQUAL(request.size(), 3); it = request.begin();
    ATEST_EQUAL((*it), 1); ++it; ++it;
    ATEST_EQUAL((*it), 3);
    request.clear();
    buffer_manager->GeneratePeerRequest(2, chunk_info, &request);
    ATEST_EQUAL(request.size(), 2); it = request.begin();
    ATEST_EQUAL((*it), 4); ++ it;
    ATEST_EQUAL((*it), 5);
    RESET

    
    
    fast_delete(BufferManager, buffer_manager);
    delete _core;
}