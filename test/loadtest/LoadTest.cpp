//
//  LoadTest.cpp
//  Peer
//
//  Created by Oleg on 16.06.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "LoadTest.h"

using namespace PeerLib;
using namespace MProtoLib;
using namespace CoreObjectLib;
using namespace std;

LoadTest::LoadTest(CoreObject *core)
:_core(core),_n(0),_bind_port(1000)
{
    _connect_thread = _core->GetThread();
    _connect_thread->OnThread.Attach(this, &LoadTest::DoConnectJob);
}

LoadTest::~LoadTest()
{
    Peer *peer;
    _core->ReleaseThread(&_connect_thread);
    
    for (auto it = _peers.begin(); it != _peers.end(); ++it)
    {
        peer = (*it);
        fast_delete(peer);
    }
}

void LoadTest::Run()
{
    int n;
    
    _connect_thread->Wake();    
    do
    {
        LOG_VERBOSE("Input number of simultaneos connect steps or enter negative number to exit:");
        cin>>n;

        _lock.Lock();
        _n = n;
        _lock.UnLock();
    }
    while(n >= 0 && n < 10000);
}

void LoadTest::DoConnectJob()
{
    int n;
    Peer *peer;
    int attempts = 100;
 
    _lock.Lock();
    n = _n;
    _lock.UnLock();
    
    if (n > 0)
    {
        for (int i = 0; i < n; ++i)
        {
            fast_new(peer, _core);
            peer->OnTrackerDisconnect.Attach(this, &LoadTest::ProcessTrackerDisconnected);
            _peers.insert(peer);        
            
            for (; !peer->Bind(_bind_port); )
                ++_bind_port;
            ++_bind_port;
            
            peer->ConnectToTracker("192.168.1.5", 12345, 12466, "192.168.1.66", 12344);
//            peer->ConnectToTracker("198.7.57.154", 12345);
//            Utils::Delay(25);
//            peer->PlayStream("xcode_demo");
        }
        _peer_lock.Lock();
        _network_size += n;
        _peer_lock.UnLock();
        LOG_INFO("Network size is "<<_network_size<<" peers");
    }
    Utils::Delay(1000);
}

void LoadTest::ProcessTrackerDisconnected(MProtoLib::MSession *session)
{
    _peer_lock.Lock();
    --_network_size;
    _peer_lock.UnLock();
}