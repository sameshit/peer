//
//  TrackerPeerFunctions.h
//  Peer
//
//  Created by Oleg on 04.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__TrackerPeerFunctions__
#define __Peer__TrackerPeerFunctions__

#include "TrackerLocalNatInfo.h"

namespace PeerLib
{
    typedef CoreObjectLib::Event<>               StreamCloseEvent;
    typedef CoreObjectLib::Event<>               StreamPlayEvent;    
    
    class TrackerPeerFunctions
    :public TrackerLocalNatInfo
    {
    public:
        TrackerPeerFunctions(CoreObjectLib::CoreObject *core);
        virtual ~TrackerPeerFunctions();
        
        bool PlayStream(const char *stream_name);
        bool PlayStream(const std::string &stream_name);
        bool PlayStream(uint64_t stream_id);
        bool StopStream();
        bool GetAllStreams();
        
        StreamCloseEvent OnCloseStream;
        StreamPlayEvent  OnPlayStream;
    protected:
        void BeginPlay();
        void EndPlay();
        virtual void ResetBuffer() = 0;
        virtual void StartStatistic() = 0;
        virtual void StopStatistic() = 0;
    private:
        CoreObjectLib::TimeTask _play_stream1_task,_play_stream2_task,_close_stream_task;
        std::string _stream_name;
        uint64_t    _stream_id;
        
        void CloseStreamIfPlaying();
        void LoopPlayStream1();
        void LoopPlayStream2();
        void LoopStopStream();
    };
    
    inline bool TrackerPeerFunctions::PlayStream(const std::string &stream_name)
    {
        return PlayStream(stream_name.c_str());
    }
}

#endif /* defined(__Peer__TrackerPeerFunctions__) */
