//
//  BufferBase.h
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferBase__
#define __Peer__BufferBase__

#include "../tracker/TrackerPeer.h"

namespace PeerLib
{
    typedef std::map<uint32_t,uint8_t*,MProtoLib::OrderIntIdLess>               Buffer;
    typedef std::map<uint32_t,uint8_t*,MProtoLib::OrderIntIdLess>::iterator     BufferIterator;
    typedef std::set<uint32_t,MProtoLib::OrderIntIdLess>                        ChunkInfo;
    typedef std::set<uint32_t,MProtoLib::OrderIntIdLess>::iterator              ChunkInfoIterator;
    typedef std::pair<uint8_t*,ssize_t>                                         DataPair;
    typedef std::unordered_set<uint32_t>                                        UnOrderedChunkInfo;
    typedef std::unordered_set<uint32_t>::iterator                              UnOrderedChunkInfoIterator;
    
    class PeerSettings
    {
    public:
        bool log_peer_responses;
        bool log_streamer_responses;
        bool log_unsuccesful_responses;
        bool log_streamer_chunk_info;
        bool log_peer_chunk_info;
        bool log_request_timeout;
        bool log_incoming_requests;
        bool log_outcoming_peer_requests;
        bool log_outcoming_streamer_requests;
        bool log_unsuccessful_requests;
        bool log_statistic;
        bool log_skipped_chunks;
        bool log_hybrid_control;
        bool log_super_peer;
        
        PeerSettings()
        :log_peer_responses(false),log_streamer_responses(false),log_unsuccesful_responses(true),
        log_streamer_chunk_info(false),log_peer_chunk_info(false),log_request_timeout(false),
        log_incoming_requests(false),log_outcoming_peer_requests(false),
        log_outcoming_streamer_requests(false),log_unsuccessful_requests(false),
        log_statistic(false),log_skipped_chunks(true),log_hybrid_control(false),
        log_super_peer(false){}
        virtual ~PeerSettings(){}
    };
    
    class BufferBase
    :public TrackerPeer
    {
    public:
        BufferBase(CoreObjectLib::CoreObject *core):TrackerPeer(core){}
        virtual ~BufferBase(){}
        
        MProtoLib::ConnectEvent     OnPeerConnect,OnStreamerConnect;
        MProtoLib::DisconnectEvent  OnPeerDisconnect,OnStreamerDisconnect;
        PeerSettings* PeerSettings() {return &_peer_settings;}
    private:
        class PeerSettings _peer_settings;
        Buffer             _buffer;
        
        friend class BufferRequestTry;
        friend class BufferCongestion;
        friend class BufferProcess;
        friend class BufferChunkInfo;
        friend class BufferOutput;
        friend class BufferHybridControl;        
    };
}

#endif /* defined(__Peer__BufferBase__) */
