//
//  main.cpp
//  Peer
//
//  Created by Oleg on 22.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "../src/peer/PeerBase.h"

using namespace CoreObjectLib;

int main()
{
    return ATest::RunAll() ? 0:1;
}
