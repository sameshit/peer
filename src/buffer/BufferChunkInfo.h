//
//  BufferChunkInfo.h
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferChunkInfo__
#define __Peer__BufferChunkInfo__

#include "BufferProcess.h"

namespace PeerLib
{
    class BufferChunkInfo
    :public BufferProcess
    {
    public:
        BufferChunkInfo(CoreObjectLib::CoreObject *core);
        virtual ~BufferChunkInfo();
    private:
        uint8_t _chunk_info_data[1300];
        CoreObjectLib::Timer *_chunk_info_timer;
        
        void UpdateChunkInfoCounter();
        uint32_t _chunks_counter;
        void SendChunkInfoToAll();
        MPROTO_CONNECT_PROC(SendChunkInfo);
        DataPair SerializeChunkInfo();        
    };
}

#endif /* defined(__Peer__BufferChunkInfo__) */