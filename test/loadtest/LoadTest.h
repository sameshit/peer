//
//  LoadTest.h
//  Peer
//
//  Created by Oleg on 16.06.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__LoadTest__
#define __Peer__LoadTest__

#include "../../src/Peer.h"

namespace PeerLib
{
    class LoadTest
    {
    public:
        LoadTest(CoreObjectLib::CoreObject *core);
        virtual ~LoadTest();
        
        void Run();
    private:
        int _n;
        uint16_t _bind_port;
        ssize_t _network_size;
        CoreObjectLib::CoreObject *_core;
        CoreObjectLib::Thread *_connect_thread;
        CoreObjectLib::SpinLock _lock,_peer_lock;
        std::unordered_set<Peer*> _peers;
        
        void DoConnectJob();
        void ProcessTrackerDisconnected(MProtoLib::MSession *session);
    };
}

#endif /* defined(__Peer__LoadTest__) */
