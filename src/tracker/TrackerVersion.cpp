//
//  TrackerVersion.cpp
//  Streamer
//
//  Created by Oleg on 21.06.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerVersion.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

TrackerVersion::TrackerVersion(CoreObject *core)
:TrackerRecv(core)
{
    
}

TrackerVersion::~TrackerVersion()
{
    
}

void TrackerVersion::SendTrackerProtocolVersion()
{
    uint8_t data[5];
    
    Utils::PutByte(data, (uint8_t)MTReqVersion);
    Utils::PutBe32(data+1,kTrackerProtocolVersion);
    SendReliable(GetTrackerSession(), data, 5);
}

void TrackerVersion::ProcessInvalidVersion(uint8_t *data, ssize_t size)
{
    OnInvalidVersion();
    
    GetTrackerSession()->GetDisconnectReason() << "Protocol has invalid version, please update software";
    Disconnect(GetTrackerSession());
}