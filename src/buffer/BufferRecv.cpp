//
//  BufferRecv.cpp
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferRecv.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

BufferRecv::BufferRecv(CoreObjectLib::CoreObject *core)
:BufferRegister(core)
{

}

BufferRecv::~BufferRecv()
{

}

void BufferRecv::ProcessPeerNonReliable(MSession *session, uint8_t *data, ssize_t size)
{
    PeerMessageType type;
    
    if (size < 1)
    {
        session->GetDisconnectReason() << "Invalid size("<<size<<") of non-reliable message from peer";
        Disconnect(session);
        return;
    }
    
    type = (PeerMessageType)data[0];
    switch (type)
    {
        case PeerMessageBufferInfo:
            DeserializePeerChunkInfo(session, data+1, size - 1);
            break;
        case PeerMessageRequest:
            DeserializePeerRequest(session,data+1,size-1);
            break;
        case PeerMessageResponse:
            DeserializeResponse(session, data+1, size-1);
            break;
        default:
            session->GetDisconnectReason() << "Invalid non reliable message type("<<type<<") from peer";
            Disconnect(session);
            break;
    }
}

void BufferRecv::ProcessStreamerNonReliable(MSession *session, uint8_t *data, ssize_t size)
{
    PeerMessageType type;
    
    if (size < 1)
    {
        session->GetDisconnectReason() << "Invalid size("<<size<<") of non-reliable message from streamer";
        Disconnect(session);
        return;
    }
    
    type = (PeerMessageType)data[0];
    switch (type)
    {
        case PeerMessageBufferInfo:
            DeserializeStreamerChunkInfo(session, data+1, size-1);
            break;
        case PeerMessageResponse:
            DeserializeResponse(session, data+1, size-1);
            break;
        default:
            session->GetDisconnectReason() << "Invalid non reliable message type("<<type<<") from streamer";
            Disconnect(session);
            break;
    }
}