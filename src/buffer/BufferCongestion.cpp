//
//  BufferCongestion.cpp
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferCongestion.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace std;
using namespace MProtoLib;

const ssize_t kMaxRequestSize = 200;
const uint32_t kMinRequestSize = 3;
int myrandom(int i) {return rand() % i;}

BufferCongestion::BufferCongestion(CoreObject *core)
:BufferRequestTry(core)
{

}

BufferCongestion::~BufferCongestion()
{

}

void BufferCongestion::CongestionControl(MSession *session, TotalRequestInfo *total_request_info,
                                         RequestInfo *request_info, const CongestionState &congestion_state)
{
    ClearNotResponsedChunks(request_info);
    GenerateMaxRequestSize(request_info, congestion_state);
    if (session == GetStreamerSession())
    {
        GenerateStreamerRequest(request_info);                
        if (PeerSettings()->log_outcoming_streamer_requests)
            PrintRequest(GetStreamerSession(), request_info->requested_chunks);
    }
    else
    {
        GeneratePeerRequest(session,request_info);
        if (PeerSettings()->log_outcoming_streamer_requests)
            PrintRequest(session
                         , request_info->requested_chunks);
    }
    SendRequest(session, request_info);
    UpdateIsFirstRequest(total_request_info, request_info);
}

void BufferCongestion::ClearNotResponsedChunks(RequestInfo *request_info)
{
    ChunkInfo not_responsed_chunks;
    OrderIntIdLess less;
    
    set_difference(request_info->requested_chunks.begin(), request_info->requested_chunks.end(),
                   request_info->responsed_chunks.begin(), request_info->responsed_chunks.end(),
                   inserter(not_responsed_chunks,not_responsed_chunks.begin()),
                   less);
    
    for (auto it = not_responsed_chunks.begin();
         it != not_responsed_chunks.end();
         ++it)
        _requested_chunks.erase((*it));
}

void BufferCongestion::GenerateMaxRequestSize(RequestInfo *request_info,
                                              const CongestionState &congestion_state)
{
    switch(congestion_state)
    {
        case CongestionReset:
            request_info->max_request_size = kMinRequestSize;
            request_info->fast_start = true;
            break;
        case CongestionIncrease:
            if (request_info->fast_start)
                request_info->max_request_size = 2*(uint32_t)request_info->requested_chunks.size();
            else
                request_info->max_request_size = 1+(uint32_t)request_info->requested_chunks.size();
            break;
        case CongestionDecrease:
            request_info->fast_start = false;
            request_info->max_request_size = request_info->requested_chunks.size() / 2;
            break;
        default:
			FATAL_ERROR("Invalid congestion state("<<congestion_state<<")");
    }
    
    if (request_info->max_request_size  < kMinRequestSize)
        request_info->max_request_size  = kMinRequestSize;
    
    request_info->requested_chunks.clear();
    request_info->responsed_chunks.clear();
}

bool BufferCongestion::HasPeerChunk(const uint32_t &chunk_id)
{
    MSession *session;
    PeerInfo *peer_info;
    OrderIntIdLessOrEqual less_eq;
    
    for (auto it = SessionsBegin(); it != SessionsEnd(); ++it)
    {
        session = (*it).second;
        if (session == GetTrackerSession() || session == GetStreamerSession() ||
            session->GetPackedAddr() == GetStun1Addr() || session->GetPackedAddr() == GetStun2Addr())
            continue;
        
        peer_info = GET_PEER_INFO(session);
        if ((less_eq(peer_info->first_id,chunk_id) && less_eq(chunk_id,peer_info->last_id)) ||
            peer_info->chunk_info.count(chunk_id) == 1)
            return true;
    }
    return false;
}

void BufferCongestion::GenerateStreamerRequest(RequestInfo *request_info)
{
    ChunkInfo can_request;
    ChunkInfoIterator sec_it1,sec_it2,insert_it;
    StreamerInfo *streamer_info;
    OrderIntIdLess less;
    uint32_t i;
    
    streamer_info = GET_STREAMER_INFO(GetStreamerSession());
    
    if(streamer_info->seconds.size() < 2)
        return;
    if (!streamer_info->is_streamer_downloading)
        return;
    
    insert_it = can_request.begin();
    for (sec_it1 = streamer_info->seconds.begin(),sec_it2 = sec_it1,++sec_it2;
         sec_it2 != streamer_info->seconds.end() && sec_it1!=streamer_info->seconds.end() && request_info->max_request_size > 0;
         sec_it1 = sec_it2, ++sec_it2)
    {
        if (less(_next_request_id,(*sec_it2)))
        {
            i = less((*sec_it1),_next_request_id) ? _next_request_id : (*sec_it1);
            for (; less(i,(*sec_it2)); ++i)
            {
 //               assert(_next_request_id != i);
                if (_requested_chunks.find(i) == _requested_chunks.end() && !HasPeerChunk(i))
                    insert_it = can_request.insert(insert_it,i);
                if (can_request.size() > 100)
                    break;
            }
            RandomizeRequest(request_info, can_request);
            can_request.clear();
            insert_it = can_request.begin();
        }
    }
}

void BufferCongestion::GeneratePeerRequest(MSession *session, RequestInfo *request_info)
{
    ChunkInfo can_request;
    ChunkInfoIterator sec_it1,sec_it2,insert_it,chunk_it1,chunk_it2;
    StreamerInfo *streamer_info;
    PeerInfo *peer_info;
    OrderIntIdLess less;
    OrderIntIdLessOrEqual less_eq;
    uint32_t i,j;
    
    streamer_info   = GET_STREAMER_INFO(GetStreamerSession());
    peer_info       = GET_PEER_INFO(session);
        
    insert_it = can_request.begin();
    for (sec_it1 = streamer_info->seconds.begin(),sec_it2 = sec_it1,++sec_it2;
         sec_it2 != streamer_info->seconds.end() && request_info->max_request_size > 0;
         sec_it1 = sec_it2, ++sec_it2)
    {
        if (less(_next_request_id,(*sec_it2)))
        {
            i = less((*sec_it1),_next_request_id) ? _next_request_id : (*sec_it1);
            i = less(peer_info->first_id,i) ? i : peer_info->first_id;
            j = less(peer_info->last_id,(*sec_it2)) ? peer_info->last_id : (*sec_it2);
            for (;less_eq(i,j);++i)
                if (_requested_chunks.find(i) == _requested_chunks.end())
                    insert_it = can_request.insert(insert_it,i);
            
            insert_it = can_request.begin();
            i = less((*sec_it1),_next_request_id) ? _next_request_id : (*sec_it1);
//            chunk_it1 = peer_info->chunk_info.lower_bound(i);
//            chunk_it2 = peer_info->chunk_info.upper_bound((*sec_it2));
//            for (;chunk_it1 != chunk_it2; ++chunk_it1)
            
            RandomizeRequest(request_info, can_request);
            can_request.clear();
            insert_it = can_request.begin();
        }
    }
    
    for (auto chunk_it = peer_info->chunk_info.begin();
         chunk_it != peer_info->chunk_info.end();
         ++chunk_it)
    if (_requested_chunks.find((*chunk_it)) == _requested_chunks.end())
        insert_it = can_request.insert(insert_it,(*chunk_it));
    
    
    RandomizeRequest(request_info, can_request);
}

void BufferCongestion::RandomizeRequest(RequestInfo *request_info,const ChunkInfo &can_request)
{
    vector<bool> shuffle_vector;
    ssize_t i;
    ChunkInfoIterator chunk_it,insert_it;
    UnOrderedChunkInfoIterator requested_chunks_insert_it;
    
    shuffle_vector.resize(can_request.size());
    for (i = 0;
         i < can_request.size();
         ++i)
    {
        if (i < request_info->max_request_size)
            shuffle_vector[i] = true;
        else
            shuffle_vector[i] = false;
    }
    
    random_shuffle(shuffle_vector.begin(),shuffle_vector.end(),myrandom);
    
    insert_it = request_info->requested_chunks.begin();
    requested_chunks_insert_it = _requested_chunks.begin();
    for (i = 0,chunk_it = can_request.begin();
         chunk_it != can_request.end();
         ++chunk_it, ++i)
    {
        if (shuffle_vector[i])
        {
            insert_it                   = request_info->requested_chunks.insert(insert_it,(*chunk_it));
            requested_chunks_insert_it  = _requested_chunks.insert(requested_chunks_insert_it,(*chunk_it));
        }
    }
    
    if (request_info->max_request_size < (uint32_t)can_request.size())
        request_info->max_request_size = 0;
    else
        request_info->max_request_size -= (uint32_t)can_request.size();
}

void BufferCongestion::SendRequest(MSession *session,
                                   RequestInfo *request_info)
{
    uint32_t first_id,end_id,cur_id,diff;
    ssize_t size;
    uint8_t *pos;
    PeerMessageType type;
    ChunkInfoIterator chunk_info_it;
    
    if (request_info->requested_chunks.empty())
        return;
    
    first_id = (*request_info->requested_chunks.begin());
    end_id = (*request_info->requested_chunks.rbegin());
    
    if (end_id - first_id != 0)
        size = 4 + ((end_id - first_id - 1) / 8) + 1;
    else
        size = 4;
    
    memset(_request_data,0,size);
    
    Utils::PutBe32(_request_data, first_id);
    for (chunk_info_it = request_info->requested_chunks.begin(),++chunk_info_it;
         chunk_info_it != request_info->requested_chunks.end();
         ++chunk_info_it)
    {
        cur_id = (*chunk_info_it);
        diff = cur_id - first_id - 1;
        pos = _request_data + 4 + (diff / 8);
        *pos |= 1 << (diff % 8);
    }
    
    request_info->request_time.Update();
    type = PeerMessageRequest;
    SendNonReliable(session, (uint8_t*)&type, 1, _request_data, size);
}

void BufferCongestion::UpdateIsFirstRequest(TotalRequestInfo *total_request_info, RequestInfo *request_info)
{
    if (request_info->requested_chunks.empty())
        request_info->requested = false;
    else
    {
        request_info->requested = true;
        if (&total_request_info->request1 == request_info)
        {
            if (total_request_info->request2.requested)
                total_request_info->is_first_request = false;
            else
                total_request_info->is_first_request = true;
        }
        else
        {
            if (total_request_info->request1.requested)
                total_request_info->is_first_request = true;
            else
                total_request_info->is_first_request = false;
        }        
    }
}

void BufferCongestion::ClearRequests()
{
    StreamerInfo *streamer_info;
    OrderIntIdLess less;
    UnOrderedChunkInfoIterator prev_it,cur_it;
    
    streamer_info = GET_STREAMER_INFO(GetStreamerSession());
    
    prev_it = _requested_chunks.begin();
    cur_it = prev_it;
    if (cur_it != _requested_chunks.end())
        ++cur_it;
    for (;
         cur_it != _requested_chunks.end();
         prev_it = cur_it,++cur_it)
    {
        if (less((*prev_it),(*streamer_info->seconds.begin())) )
            _requested_chunks.erase(prev_it);
    }
    if (prev_it != _requested_chunks.end() &&
        less((*prev_it),(*streamer_info->seconds.begin())) )
        _requested_chunks.erase(prev_it);
}

void BufferCongestion::ClearPeerRequest(MSession *session)
{
    PeerInfo *peer_info;
    ChunkInfo not_responsed_chunks;
    OrderIntIdLess less;
    RequestInfo *request_info;
    
    peer_info = GET_PEER_INFO(session);
    request_info = &peer_info->total_request_info.request1;
    set_difference(request_info->requested_chunks.begin(), request_info->requested_chunks.end(),
                   request_info->responsed_chunks.begin(), request_info->responsed_chunks.end(),
                   inserter(not_responsed_chunks,not_responsed_chunks.begin()),
                   less);

    request_info = &peer_info->total_request_info.request2;
    set_difference(request_info->requested_chunks.begin(), request_info->requested_chunks.end(),
                   request_info->responsed_chunks.begin(), request_info->responsed_chunks.end(),
                   inserter(not_responsed_chunks,not_responsed_chunks.begin()),
                   less);
    
    for (auto it = not_responsed_chunks.begin();
         it != not_responsed_chunks.end();
         ++it)
        _requested_chunks.erase((*it));
}

void BufferCongestion::ClearStreamerRequest(MSession *session)
{
    StreamerInfo *streamer_info;
    ChunkInfo not_responsed_chunks;
    OrderIntIdLess less;
    RequestInfo *request_info;
    
    streamer_info = GET_STREAMER_INFO(session);
    request_info = &streamer_info->total_request_info.request1;
    set_difference(request_info->requested_chunks.begin(), request_info->requested_chunks.end(),
                   request_info->responsed_chunks.begin(), request_info->responsed_chunks.end(),
                   inserter(not_responsed_chunks,not_responsed_chunks.begin()),
                   less);
    
    request_info = &streamer_info->total_request_info.request2;
    set_difference(request_info->requested_chunks.begin(), request_info->requested_chunks.end(),
                   request_info->responsed_chunks.begin(), request_info->responsed_chunks.end(),
                   inserter(not_responsed_chunks,not_responsed_chunks.begin()),
                   less);
    
    for (auto it = not_responsed_chunks.begin();
         it != not_responsed_chunks.end();
         ++it)
        _requested_chunks.erase((*it));
}

void BufferCongestion::PrintRequest(MProtoLib::MSession *session, const ChunkInfo &request)
{
    if (session != GetStreamerSession())
    {
        LOG_VERBOSE( "Making request to "<<PRINT_SESSION_ADDR(session)<<"."<<endl
        <<  "Size of request: "<<request.size()<<"."<<endl
        <<  "Next request id: "<<_next_request_id<<endl
        <<  "Data: ");
        for (auto it = request.begin(); it != request.end(); ++ it)
        {
            cout <<(*it)<<" ";
        }
        cout << endl;
    }
    else
    {
        LOG_VERBOSE( "Making request to streamer."<<endl
        <<  "Size of request: "<<request.size()<<"."<<endl
        <<  "Next request id: "<<_next_request_id<<endl        
        <<  "Data: ");
        for (auto it = request.begin(); it != request.end(); ++ it)
        {
            cout <<(*it)<<" ";
        }
        cout << endl;
    }
}