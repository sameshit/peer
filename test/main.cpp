//
//  main.cpp
//  Peer
//
//  Created by Oleg on 29.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "simpletest/SimpleTest.h"
#include "severalpeerstest/SeveralPeersTest.h"
#include "loadtest/LoadTest.h"

using namespace CoreObjectLib;
using namespace PeerLib;
using namespace std;

int main(int argc, char **argv)
{
    CoreObject *_core;
//    SeveralPeersTest *peer;
    SimpleTest *peer;
//    LoadTest *peer;
    uint16_t port;
    
    if (argc < 2)
        port = 15234;
    else
        port = atoi(argv[1]);
    
    _core = new CoreObject;
  
//    fast_new(peer,_core);
    fast_new(peer,_core,port);
//    fast_new1(SeveralPeersTest,peer,_core);
    
    peer->Run();
    cin.get();
    
    fast_delete(peer);
    delete _core;
    return 0;
}

