//
//  BufferCongestion.h
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferCongestion__
#define __Peer__BufferCongestion__

#include "BufferRequestTry.h"

namespace PeerLib
{    
    class BufferCongestion
    :public BufferRequestTry
    {
    public:
        BufferCongestion(CoreObjectLib::CoreObject *core);
        virtual ~BufferCongestion();
    private:
        UnOrderedChunkInfo _requested_chunks;
        uint8_t _request_data[1300];
        
        
        void CongestionControl(MProtoLib::MSession *session,
                               TotalRequestInfo *total_request_info,
                               RequestInfo *request_info,
                               const CongestionState &congestion_state);
        
        void ClearNotResponsedChunks(RequestInfo *request_info);
        void GenerateMaxRequestSize(RequestInfo *request_info,
                                    const CongestionState &congestion_state);
        void GeneratePeerRequest    (MProtoLib::MSession *session, RequestInfo *request_info);
        void GenerateStreamerRequest(RequestInfo *request_info);
        void RandomizeRequest(RequestInfo *request_info,const ChunkInfo &can_request);
        void SendRequest(MProtoLib::MSession *session,RequestInfo *request_info);
        void UpdateIsFirstRequest(TotalRequestInfo *total_request_info,RequestInfo *request_info);
                
        void ClearRequests();
        bool HasPeerChunk(const uint32_t &chunk_id);
        
        void ClearPeerRequest(MProtoLib::MSession *session);
        void ClearStreamerRequest(MProtoLib::MSession *session);
        void PrintRequest(MProtoLib::MSession *session,const ChunkInfo &request);
        
        friend class BufferHybridControl;
        friend class BufferRequestTimeout;
    };
}

#endif /* defined(__Peer__BufferCongestion__) */
