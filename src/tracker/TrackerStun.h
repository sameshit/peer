//
//  TrackerStun.h
//  Peer
//
//  Created by Oleg on 29.06.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__TrackerStun__
#define __Peer__TrackerStun__

#include "TrackerVersion.h"

namespace PeerLib
{
    class TrackerStun
    :public TrackerVersion
    {
    public:
        TrackerStun(CoreObjectLib::CoreObject *core);
        virtual ~TrackerStun();
    protected:
        virtual const uint64_t& GetRealAddr() = 0;
    private:
        MPROTO_CONNECT_PROC(StunConnected);
        MPROTO_DISCONNECT_PROC(StunDisconnected);
        void StunPhase2(uint8_t *data,ssize_t size);
    };
}

#endif /* defined(__Peer__TrackerStun__) */
