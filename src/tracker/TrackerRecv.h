//
//  TrackerRecv.h
//  Peer
//
//  Created by Oleg on 04.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__TrackerRecv__
#define __Peer__TrackerRecv__

#include "TrackerBase.h"

namespace PeerLib
{
    const uint32_t kTrackerProtocolVersion = 4;
    
    enum MTrackerMessageType
    {
        MTReqVersion              =1,
        MTRepInvalidVersion         ,
        
        MTReqCreateStream           ,
        MTReqCloseStream            ,
        MTReqPlayStreamById         ,
        MTReqPlayStreamByName       ,
        MTReqGetAllStreams          ,
        MTReqStopStream             ,
        MTReqLocalNatInfo           ,
        MTReqStatistic              ,
        
        MTRepStunConnect            ,
        MTRepAllStreams             ,
        MTRepStreamer               ,
        MTRepNeighborChange         ,
        MTRepStreamClosed           ,
        MTRepRealAddr               ,
        MTRepSuperPeerEnable        ,
        MTRepSuperPeerDisable       ,
        MTRepStatistic              ,
    };
    
    class TrackerRecv
    :public TrackerBase
    {
    public:
        TrackerRecv(CoreObjectLib::CoreObject *core);
        virtual ~TrackerRecv();
    protected:
        MPROTO_MESSAGE_VPROC(ProcessRepAllStreams);
        MPROTO_MESSAGE_VPROC(ProcessRepStreamClosed);
        MPROTO_MESSAGE_VPROC(ProcessRepRealAddr);
        MPROTO_MESSAGE_VPROC(ProcessRepStreamer);
        MPROTO_MESSAGE_VPROC(ProcessRepSuperPeerEnable);
        MPROTO_MESSAGE_VPROC(ProcessRepSuperPeerDisable);
        MPROTO_MESSAGE_VPROC(ProcessRepNeighborChange);
        MPROTO_MESSAGE_VPROC(ProcessRepStatistic);
        virtual void StunPhase2(uint8_t *data,ssize_t size) = 0;
    private:
        MPROTO_MESSAGE_PROC(ProcessTrackerReliableMessage);
        
        friend class TrackerMessageHandle;
    };
}

#endif /* defined(__Peer__TrackerRecv__) */
