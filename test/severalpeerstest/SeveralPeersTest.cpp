//
//  SeveralPeersTest.cpp
//  Peer
//
//  Created by Oleg on 07.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//


#include "SeveralPeersTest.h"

using namespace PeerLib;
using namespace MProtoLib;
using namespace CoreObjectLib;

SeveralPeersTest::SeveralPeersTest(CoreObject *core)
:Peer(core)
{
    OnTrackerConnect.Attach(this,&SeveralPeersTest::TrackerConnected);
    OnTrackerDisconnect.Attach(this, &SeveralPeersTest::TrackerDisconnected);
    OnStreamerConnect.Attach(this, &SeveralPeersTest::StreamerConnected);
    OnStreamerDisconnect.Attach(this, &SeveralPeersTest::StreamerDisconnected);
    OnPeerConnect.Attach(this,&SeveralPeersTest::PeerConnected);
    OnPeerDisconnect.Attach(this, &SeveralPeersTest::PeerDisconnected);
    
    _reconnect_task.SetTimeout(2000);
    _reconnect_task.OnTimeTask.Attach(this, &SeveralPeersTest::Run);
    
    OnPlayStream.Attach(this, &SeveralPeersTest::StreamPlayed);
    OnCloseStream.Attach(this, &SeveralPeersTest::StreamClosed);
    
    //    PeerSettings()->log_unsuccesful_responses = false;
//    PeerSettings()->log_streamer_chunk_info  = true;
//    PeerSettings()->log_outcoming_requests   = true;
      PeerSettings()->log_incoming_requests = true;
//      PeerSettings()->log_responses = true;
//    PeerSettings()->log_statistic = true;
//    PeerSettings()->log_peer_chunk_info = true;
    
    fast_new(_peer1,_core);
    fast_new(_peer2,_core);
//    _peer1->PeerSettings()->log_incoming_requests = true;
//    _peer2->PeerSettings()->log_incoming_requests = true;
    
    
    LOG_INFO("this: "<<this<<", peer1: "<<_peer1<<", peer2: "<<_peer2);
}

SeveralPeersTest::~SeveralPeersTest()
{
    fast_delete(_peer1);
    fast_delete(_peer2);
}

void SeveralPeersTest::Run()
{
    _peer1->Bind(15200);
    _peer1->Connect("127.0.0.1", 12345);
    _peer1->ConnectToTracker("192.168.1.5", 12345, 12466, "192.168.1.66", 12344);
    
    _peer2->Bind(15201);
    _peer1->Connect("127.0.0.1", 12345);
    _peer1->ConnectToTracker("192.168.1.5", 12345, 12466, "192.168.1.66", 12344);
    Bind(15234);
    ConnectToTracker("192.168.1.5", 12345, 12466, "192.168.1.66", 12344);
}

void SeveralPeersTest::TrackerConnected(MSession *session)
{
    LOG_INFO("Connected to tracker "<<PRINT_SESSION_ADDR(session) );
    PlayStream("xcode_demo");
    
    _peer1->PlayStream("xcode_demo");
    _peer2->PlayStream("xcode_demo");
    
//    session->SetConnectionTimeout(1000*60*60);
}

void SeveralPeersTest::TrackerDisconnected(MSession *session)
{
    LOG_INFO("Disconnected from tracker "<<PRINT_SESSION_ADDR(session)<<" with reason: "<<
    session->GetDisconnectReason().str()<<". Reconnecting in 2 seconds." );
    _core->GetScheduler()->Add(&_reconnect_task);
}

void SeveralPeersTest::StreamerConnected(MSession *session)
{
    LOG_INFO( "Streamer "<<PRINT_SESSION_ADDR(session)<<" connected");
    
//    session->SetConnectionTimeout(1000*60*60);
}

void SeveralPeersTest::StreamerDisconnected(MSession *session)
{
    LOG_INFO("Streamer "<<PRINT_SESSION_ADDR(session)<<" disconnected with reason: "
    <<session->GetDisconnectReason().str());
}

void SeveralPeersTest::PeerConnected(MSession *session)
{
    LOG_INFO("Peer "<<PRINT_SESSION_ADDR(session)<<" connected");
    
//    session->SetConnectionTimeout(1000*60*60);
}

void SeveralPeersTest::PeerDisconnected(MSession *session)
{
    LOG_INFO("Peer "<<PRINT_SESSION_ADDR(session)<< " disconnected with reason: "
    <<session->GetDisconnectReason().str());
}

void SeveralPeersTest::StreamPlayed()
{
    LOG_INFO("Stream started to play");
}

void SeveralPeersTest::StreamClosed()
{
    LOG_INFO("Stream closed");
}