//
//  BufferRequestTimeout.cpp
//  Peer
//
//  Created by Oleg on 04.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferRequestTimeout.h"

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

BufferRequestTimeout::BufferRequestTimeout(CoreObject *core)
:BufferCongestion(core)
{
    fast_new(_request_timeout_timer,_core,200);
    _request_timeout_timer->OnTimer.Attach(this, &BufferRequestTimeout::CheckAllRequestsTimeout);
}

BufferRequestTimeout::~BufferRequestTimeout()
{
    PeerDeleteRequestTimer();
}

void BufferRequestTimeout::CheckAllRequestsTimeout()
{
    MSession *session;
    StreamerInfo *streamer_info;
    PeerInfo *peer_info;
    CongestionState congestion_state;
    COTime now;
    TotalRequestInfo *total_request_info;
    uint64_t timeout;
    
    congestion_state = CongestionReset;
    for (auto it=SessionsBegin(); it !=SessionsEnd(); ++it)
    {
        session = (*it).second;
        if (session == GetTrackerSession() ||
            session->GetPackedAddr() == GetStun1Addr() ||
            session->GetPackedAddr() == GetStun2Addr())
            continue;
        if (session == GetStreamerSession())
        {
            streamer_info = GET_STREAMER_INFO(session);
            total_request_info = &streamer_info->total_request_info;
        }
        else
        {
            peer_info = GET_PEER_INFO(session);
            total_request_info = &peer_info->total_request_info;
        }
        
        timeout = session->GetRtt()+100;
        if (total_request_info->request1.requested &&
            now - total_request_info->request1.request_time > timeout)
        {
            if (PeerSettings()->log_request_timeout)
                LOG_VERBOSE("Request 1 to "<<PRINT_SESSION_ADDR(session)<<" timeouted");
            CongestionControl(session,total_request_info,&total_request_info->request1, congestion_state);
        }
        if (total_request_info->request2.requested &&
            now - total_request_info->request2.request_time > timeout)
        {
            if (PeerSettings()->log_request_timeout)
                LOG_VERBOSE("Request 2 to "<<PRINT_SESSION_ADDR(session)<<" timeouted");
            CongestionControl(session,total_request_info,&total_request_info->request2, congestion_state);
        }
    }
}

void BufferRequestTimeout::PeerDeleteRequestTimer()
{
    if (_request_timeout_timer != NULL)
    {
        fast_delete(_request_timeout_timer);
        _request_timeout_timer = NULL;
    }
}