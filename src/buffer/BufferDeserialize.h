//
//  BufferDeserialize.h
//  Peer
//
//  Created by Oleg on 02.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferDeserialize__
#define __Peer__BufferDeserialize__

#include "BufferRecv.h"

namespace PeerLib
{
    class BufferDeserialize
    :public BufferRecv
    {
    public:
        BufferDeserialize(CoreObjectLib::CoreObject *core);
        virtual ~BufferDeserialize();
    private:
        MPROTO_MESSAGE_PROC(DeserializePeerChunkInfo);
        MPROTO_MESSAGE_PROC(DeserializePeerRequest);;
        MPROTO_MESSAGE_PROC(DeserializeStreamerChunkInfo);
        MPROTO_MESSAGE_PROC(DeserializeResponse);
    protected:
        virtual void ProcessPeerRequest(MProtoLib::MSession *session,
                                        const ChunkInfo &chunk_info) = 0;
        virtual void ProcessResponse(MProtoLib::MSession *session,
                                         const uint32_t& chunk_id,const uint8_t *chunk_data) = 0;
        virtual void TryRequest(MProtoLib::MSession *session, TotalRequestInfo *total_request_info) = 0;
        virtual void SendChunkInfoToAll() = 0;
        virtual void ClearBuffer() = 0;
        virtual void ClearRequests() = 0;
        virtual void TryHybridControl() = 0;
        
        friend class BufferChunkInfo;
    };
    
    const ssize_t   kChunkSize = 1300;
}

#endif /* defined(__Peer__BufferDeserialize__) */
