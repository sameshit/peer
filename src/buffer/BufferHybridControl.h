//
//  BufferHybridControl.h
//  Peer
//
//  Created by Oleg on 15.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__BufferHybridControl__
#define __Peer__BufferHybridControl__

#include "BufferRequestTimeout.h"

namespace PeerLib
{
    class BufferHybridControl
    :public BufferRequestTimeout
    {
    public:
        BufferHybridControl(CoreObjectLib::CoreObject *core);
        virtual ~BufferHybridControl();
    private:
        CoreObjectLib::TimeTask _hybrid_control_task;
        CoreObjectLib::Timer *_hybrid_control_timer;
        bool _is_super_peer;        
        
        MPROTO_MESSAGE_PROC(ProcessRepSuperPeerEnable);
        MPROTO_MESSAGE_PROC(ProcessRepSuperPeerDisable);
        void HybridControl();
        void TryHybridControl();
        void ProcessTryHybridControl();
        void ResetSuperPeer();
        
        ssize_t GetBufferInSeconds();
    };
}

#endif /* defined(__Peer__BufferHybridControl__) */
