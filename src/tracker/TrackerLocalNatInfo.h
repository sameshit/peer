//
//  TrackerLocalNatInfo.h
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__TrackerLocalNatInfo__
#define __Streamer__TrackerLocalNatInfo__

#include "TrackerStun.h"

namespace PeerLib
{
    class TrackerLocalNatInfo
    :public TrackerStun
    {
    public:
        TrackerLocalNatInfo(CoreObjectLib::CoreObject *core);
        virtual ~TrackerLocalNatInfo();
    private:
        void ProcessLocalNatInfo(MProtoLib::MSession *session);
    };
}

#endif /* defined(__Streamer__TrackerLocalNatInfo__) */
