//
//  TrackerLocalNatInfo.cpp
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerLocalNatInfo.h"

#if defined(OS_X) || defined(OS_LINUX)
#include <ifaddrs.h>
#elif defined(OS_WIN)
#include <iphlpapi.h>
#endif

using namespace PeerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

const uint8_t secret_key[4] = {99,255,17,111};

TrackerLocalNatInfo::TrackerLocalNatInfo(CoreObject *core)
:TrackerStun(core)
{

}

TrackerLocalNatInfo::~TrackerLocalNatInfo()
{

}

void TrackerLocalNatInfo::ProcessLocalNatInfo(MSession *session)
{
    uint8_t data[1299], *pos,secret[4],*not_secret;
    ssize_t size;
	MTrackerMessageType type;
#if defined(OS_WIN)
	unsigned long local_addr;
#else
    in_addr_t local_addr;
#endif

	type = MTReqLocalNatInfo;
    Utils::PutByte(data, type);
    pos = data + 1;
    memcpy(pos,(void*)&_addr.sin_port,2);
    pos += 2;
    size = 3;
	local_addr = inet_addr("127.0.0.1");

#if defined(OS_LINUX) || defined(OS_X)
    struct ifaddrs *ifaddr_all,*ifa;
    struct sockaddr_in *addr,*net_mask,subnet;
    
    getifaddrs(&ifaddr_all);
    
    for (ifa = ifaddr_all; ifa != NULL && pos - data < 1291; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr->sa_family==AF_INET)
        {
            addr =(struct sockaddr_in *)ifa->ifa_addr;
            if (addr->sin_addr.s_addr!=local_addr)
            {
                net_mask=(struct sockaddr_in *)ifa->ifa_netmask;
                subnet.sin_addr.s_addr = addr->sin_addr.s_addr & net_mask->sin_addr.s_addr;

				not_secret	= (uint8_t*)&addr->sin_addr.s_addr;
				for (auto j = 0; j < 4; ++j)
					secret[j] = not_secret[j] ^ secret_key[j];
				memcpy(pos  , secret, 4);

				not_secret	= (uint8_t*)&subnet.sin_addr.s_addr;
				for (auto j = 0; j < 4; ++j)
					secret[j] = not_secret[j] ^ secret_key[j];
				memcpy(pos +4 , secret, 4);
                
                pos += 8;
                size+= 8;
            }
        }
    }
    if (ifaddr_all!=NULL)
        freeifaddrs(ifaddr_all);

#elif defined(OS_WIN)
	PMIB_IPADDRTABLE addr_table;
	DWORD ret_val,sub_net;
	LPVOID err_msg;
	ULONG addr_size;

	addr_size = 0;
	fast_alloc(addr_table,1);
	if (GetIpAddrTable(addr_table, &addr_size, 0)==ERROR_INSUFFICIENT_BUFFER)
	{
		fast_free(addr_table);
		fast_alloc(addr_table,addr_size);
    }

	if ((ret_val = GetIpAddrTable(addr_table, &addr_size, 0))!=NO_ERROR)
	{
		if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS
			, NULL, ret_val, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),       // Default language
            (LPTSTR) & err_msg, 0, NULL)) 
		{
            LOG_ERROR("GetIpAddrTable returned error("<<ret_val<<"): "<<(char*)err_msg);
            LocalFree(err_msg);
			LOG_FLUSH();
			abort();
		}
		FATAL_ERROR("GetIpAddrTable returned error("<<ret_val<<")");
	}

	for (auto i=0; i < (int) addr_table->dwNumEntries; ++i)
	{
		if (addr_table->table[i].dwAddr != local_addr)
		{
			not_secret	= (uint8_t*)&addr_table->table[i].dwAddr;
			for (auto j = 0; j < 4; ++j)
				secret[j] = not_secret[j] ^ secret_key[j];
			memcpy(pos  , secret, 4);

			sub_net = addr_table->table[i].dwAddr & addr_table->table[i].dwMask;
			not_secret	= (uint8_t*)&sub_net;
			for (auto j = 0; j < 4; ++j)
				secret[j] = not_secret[j] ^ secret_key[j];
			memcpy(pos +4 , secret, 4);

            pos += 8;
            size+= 8;
		}
	}
	fast_free(addr_table);
#endif
	if (size != 3)
        SendReliable(session, data, size);
    else
        SendReliable(session, (uint8_t*)&type, 1);
}