//
//  TrackerMessageHandle.h
//  Peer
//
//  Created by Oleg on 04.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Peer__TrackerMessageHandle__
#define __Peer__TrackerMessageHandle__

#include "TrackerPeerFunctions.h"

namespace PeerLib
{
    typedef CoreObjectLib::Event<std::set<uint64_t> >     AllStreamsEvent;
    
    class TrackerMessageHandle
    :public TrackerPeerFunctions
    {
    public:
        TrackerMessageHandle(CoreObjectLib::CoreObject *core);
        virtual ~TrackerMessageHandle();
        
        AllStreamsEvent  OnAllStreams;
    protected:
        inline const uint64_t& GetRealAddr() {return _real_addr;}
    private:
        void ProcessRepAllStreams(MProtoLib::MSession *session, uint8_t *data,ssize_t size);
        void ProcessRepRealAddr(MProtoLib::MSession *session, uint8_t *data,ssize_t size);
        void ProcessRepStreamClosed(MProtoLib::MSession *session, uint8_t *data, ssize_t size);
        void ProcessRepNeighborChange(MProtoLib::MSession *session, uint8_t *data,ssize_t size);
        void ProcessRepStreamer(MProtoLib::MSession *session, uint8_t *data,ssize_t size);
        
        uint64_t _real_addr;
        
        friend class BufferRegister;
    };
}

#endif /* defined(__Peer__TrackerMessageHandle__) */
