//
//  BufferStatistic.inl
//  Peer
//
//  Created by Oleg on 07.05.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef Peer_BufferStatistic_inl
#define Peer_BufferStatistic_inl

inline uint64_t BufferStatistic::GetCurrentPeerDownload()
{
    return _current_peer_download;
}

inline uint64_t BufferStatistic::GetCurrentStreamerDownload()
{
    return _current_streamer_download;
}

inline uint64_t BufferStatistic::GetCurrentEconomy()
{
    if (_current_peer_download + _current_streamer_download == 0)
        return 0;
    else
        return (_current_peer_download*100)/(_current_peer_download +_current_streamer_download);
}

inline uint64_t BufferStatistic::GetTotalPeerDownload()
{
    return _total_peer_download;
}

inline uint64_t BufferStatistic::GetTotalStreamerDownload()
{
    return _total_streamer_download;
}

inline uint64_t BufferStatistic::GetTotalEconomy()
{
    if (_total_peer_download + _total_streamer_download == 0)
        return 0;
    else
        return (_total_peer_download*100)/(_total_peer_download + _total_streamer_download);
}

inline void BufferStatistic::LogStatistic()
{
    LOG_VERBOSE( std::endl<<"Cur peer dl: "<<GetCurrentPeerDownload()/1024<<" kb"
    <<", cur streamer dl: "<<GetCurrentStreamerDownload()/1024<<" kb"
    <<", cur eco: "<<GetCurrentEconomy()<<"%."<<std::endl
    <<"Total peer dl: "<<GetTotalPeerDownload()/1024<<" kb"
    <<", total streamer dl: "<<GetTotalStreamerDownload()/1024<<" kb"
    <<", total eco: "<<GetTotalEconomy()<<"%."<<std::endl
    <<", next request id: "<<_next_request_id);
}

inline void BufferStatistic::StartStatistic()
{
    _current_streamer_download = 0;
    _current_peer_download = 0;
    _total_streamer_download = 0;
    _total_peer_download = 0;
    _statistic_timer_id = _statistic_timer->OnTimer.Attach(this, &BufferStatistic::CalculateStatistic);
}

inline void BufferStatistic::StopStatistic()
{
    _statistic_timer->OnTimer.Deattach(_statistic_timer_id);
}

inline void BufferStatistic::UpdatePeerStatistic()
{
    _current_peer_download += kChunkSize;
    _tracker_peer_download += kChunkSize;
}

inline void BufferStatistic::UpdateStreamerStatistic()
{
    _current_streamer_download += kChunkSize;
    _tracker_streamer_download += kChunkSize;
}

#endif
