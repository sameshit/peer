//
//  TrackerPeerFunctions.cpp
//  Peer
//
//  Created by Oleg on 04.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerPeerFunctions.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace PeerLib;
using namespace std;

TrackerPeerFunctions::TrackerPeerFunctions(CoreObject *core)
:TrackerLocalNatInfo(core)
{
    _play_stream1_task.OnTimeTask.Attach(this,&TrackerPeerFunctions::LoopPlayStream1);
    _play_stream2_task.OnTimeTask.Attach(this,&TrackerPeerFunctions::LoopPlayStream2);
    _close_stream_task.OnTimeTask.Attach(this,&TrackerPeerFunctions::LoopStopStream);
}

TrackerPeerFunctions::~TrackerPeerFunctions()
{

}

bool TrackerPeerFunctions::PlayStream(const char *stream_name)
{
    if (_tracker_state != TrackerConnected)
    {
        COErr::Set("Invalid peer state");
        return false;
    }
    
    _stream_name.assign(stream_name);
    _tracker_state = TrackerPlayingAndWaitingForStreamer;
    
    if (_core->IsLoopThread())
        LoopPlayStream1();
    else
        _core->GetScheduler()->Add(&_play_stream1_task);
    return true;
}

void TrackerPeerFunctions::LoopPlayStream1()
{
    uint8_t *data;
    ssize_t size;
    MTrackerMessageType type;
    
    type = MTReqPlayStreamByName;
    size = _stream_name.size() + 1;
    
    fast_alloc(data, size);
    Utils::PutByte(data, type);
    memcpy(data + 1, _stream_name.c_str(),_stream_name.size());
    SendReliable(_tracker_session, data, size);
    fast_free(data);
}

bool TrackerPeerFunctions::PlayStream(uint64_t stream_id)
{
    if (_tracker_state != TrackerConnected)
    {
        COErr::Set("Invalid peer state");
        return false;
    }
    
    _stream_id = stream_id;
    _tracker_state = TrackerPlayingAndWaitingForStreamer;
    
    if (_core->IsLoopThread())
        LoopPlayStream2();
    else
        _core->GetScheduler()->Add(&_play_stream2_task);

    return true;
}

void TrackerPeerFunctions::LoopPlayStream2()
{
    uint8_t data[9];
    MTrackerMessageType type;
    
    type = MTReqPlayStreamById;
    Utils::PutByte(data, type);
    Utils::PutBe64(data+1, _stream_id);
    SendReliable(_tracker_session, data, 9);
}

void TrackerPeerFunctions::BeginPlay()
{
    StartStatistic();
    OnPlayStream();
}

void TrackerPeerFunctions::EndPlay()
{
    uint8_t data[1];
    
    if (_tracker_state != TrackerNotConnected)
    {
        Utils::PutByte(data, MTReqStopStream);
        SendReliable(_tracker_session, data, 1);
    }
    
    ResetBuffer();
    StopStatistic();
    OnCloseStream();
}

bool TrackerPeerFunctions::StopStream()
{    
    if (_tracker_state < TrackerPlayingAndWaitingForStreamer && _tracker_state > TrackerPlaying)
    {
        COErr::Set("Invalid peer state");
        return false;
    }

    if (_core->IsLoopThread())
        LoopStopStream();
    else
        _core->GetScheduler()->Add(&_close_stream_task);
    return true;
}

void TrackerPeerFunctions::LoopStopStream()
{
    Disconnect(_streamer_session);
}

bool TrackerPeerFunctions::GetAllStreams()
{
    uint8_t data[1];
    
    if (_tracker_state == TrackerNotConnected)
    {
        COErr::Set("You're not connected to tracker");
        return false;
    }
    
    Utils::PutByte(data, MTReqGetAllStreams);
    SendReliable(_tracker_session, data, 1);
    
    return true;
}