//
//  TrackerRecv.cpp
//  Peer
//
//  Created by Oleg on 04.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerRecv.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace PeerLib;
using namespace std;

TrackerRecv::TrackerRecv(CoreObject *core)
:TrackerBase(core)
{
    
}

TrackerRecv::~TrackerRecv()
{

}

void TrackerRecv::ProcessTrackerReliableMessage(MSession *session, uint8_t *data, ssize_t size)
{
    MTrackerMessageType type;
    
    if (size < 1)
    {
        session->GetDisconnectReason()<<"Invalid size("<<size<<") while processing tracker message";
        Disconnect(session);
        return;
    }
    
    type = (MTrackerMessageType)data[0];
    
    switch (type)
    {
        case MTRepAllStreams:
            ProcessRepAllStreams(session,data+1,size-1);
        break;
        case MTRepRealAddr:
            ProcessRepRealAddr(session,data+1,size-1);
        break;
        case MTRepNeighborChange:
            ProcessRepNeighborChange(session, data+1, size-1);
        break;
        case MTRepStatistic:
            ProcessRepStatistic(session, data+1, size-1);
        break;
        case MTRepStreamClosed:
            ProcessRepStreamClosed(session,data+1,size-1);
        break;
        case MTRepStunConnect:
            StunPhase2(data+1, size-1);
        break;
        case MTRepStreamer:
            ProcessRepStreamer(session,data+1,size-1);
        break;
        case MTRepSuperPeerEnable:
            ProcessRepSuperPeerEnable(session,data+1,size-1);
        break;
        case MTRepSuperPeerDisable:
            ProcessRepSuperPeerDisable(session, data+1, size-1);
        break;
        default:
            session->GetDisconnectReason()<<"Invalid message type("<<type<<") while processing tracker message";
            Disconnect(session);
        break;
    }
}